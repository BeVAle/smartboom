<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeFkAndAddColumnInProductType extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('product_types', function (Blueprint $table) {
            $table->boolean('popular')->default(0);
        });

        Schema::table('product_characteristics', function ($table) {
            $table->dropForeign('product_characteristics_characteristic_value_id_foreign');
        });

        Schema::table('product_characteristics', function (Blueprint $table) {
            $table->foreign('characteristic_value_id')->references('id')->on('characteristic_values');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('product_types', function (Blueprint $table) {
            $table->dropColumn('popular');
        });

        Schema::table('product_characteristics', function ($table) {
            $table->dropForeign('product_characteristics_characteristic_value_id_foreign');
        });

        Schema::table('product_characteristics', function (Blueprint $table) {
            $table->foreign('characteristic_value_id')->references('id')->on('products');
        });
    }
}
