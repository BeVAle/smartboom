<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class DeleteStorageAndMakeNullableOldPrice extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('products', function (Blueprint $table) {
            $table->dropColumn('old_cost');
            $table->dropColumn('storage');
        });

        Schema::table('products', function (Blueprint $table) {
            $table->integer('old_cost')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('products', function (Blueprint $table) {
            $table->dropColumn('old_cost');
        });

        Schema::table('products', function (Blueprint $table) {
            $table->integer('old_cost');
            $table->integer('storage');
        });
    }
}
