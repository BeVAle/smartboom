<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrderTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->string('name')->nullable(false);
            $table->string('phone')->nullable(false);
            $table->string('email')->nullable(true);
            $table->string('city')->nullable(true);
            $table->string('street')->nullable(true);
            $table->string('house_number')->nullable(true);
            $table->integer('type_delivery')->nullable(true);
            $table->integer('type_payment')->nullable(true);
            $table->string('promo_code')->nullable(true);
            $table->float('cost')->nullable(true);
            $table->string('comments')->nullable(true);
            $table->integer('apartment')->nullable(true);

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orders');
    }
}
