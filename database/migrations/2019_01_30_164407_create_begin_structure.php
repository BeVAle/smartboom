<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBeginStructure extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('brands', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title');
            $table->string('logo');
        });

        Schema::create('product_types', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title');
            $table->string('logo');
        });

        Schema::create('products', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('brand_id')->unsigned();
            $table->integer('product_type_id')->unsigned();
            $table->string('name');
            $table->string('article');
            $table->integer('cost');
            $table->string('color');
            $table->integer('storage');
            $table->boolean('hit')->default(0);
            $table->boolean('new')->default(0);
            $table->text('description');

            $table->foreign('brand_id')->references('id')->on('brands');
            $table->foreign('product_type_id')->references('id')->on('product_types');
        });


        Schema::create('product_photos', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('product_id')->unsigned();
            $table->string('photo');

            $table->foreign('product_id')->references('id')->on('products');
        });

        Schema::create('characteristic_group_names', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title');
        });

        Schema::create('characteristic_names', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('characteristic_group_name_id')->unsigned();
            $table->string('title');

            $table->foreign('characteristic_group_name_id')->references('id')->on('characteristic_group_names');
        });

        Schema::create('characteristic_values', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('characteristic_name_id')->unsigned();
            $table->string('title');

            $table->foreign('characteristic_name_id')->references('id')->on('characteristic_names');
        });

        Schema::create('product_characteristics', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('product_id')->unsigned();
            $table->integer('characteristic_value_id')->unsigned();

            $table->foreign('product_id')->references('id')->on('products');
            $table->foreign('characteristic_value_id')->references('id')->on('products');
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('product_characteristics', function($table)
        {
            $table->dropForeign('product_characteristics_product_id_foreign');
            $table->dropForeign('product_characteristics_characteristic_value_id_foreign');
        });

        Schema::table('characteristic_values', function($table)
        {
            $table->dropForeign('characteristic_values_characteristic_name_id_foreign');
        });

        Schema::table('characteristic_names', function($table)
        {
            $table->dropForeign('characteristic_names_characteristic_group_name_id_foreign');
        });

        Schema::table('product_photos', function($table)
        {
            $table->dropForeign('product_photos_product_id_foreign');
        });

        Schema::table('products', function($table)
        {
            $table->dropForeign('products_product_type_id_foreign');
            $table->dropForeign('products_brand_id_foreign');
        });


        Schema::dropIfExists('product_characteristics');
        Schema::dropIfExists('characteristic_values');
        Schema::dropIfExists('characteristic_names');
        Schema::dropIfExists('characteristic_group_names');
        Schema::dropIfExists('product_photos');
        Schema::dropIfExists('products');
        Schema::dropIfExists('product_types');
        Schema::dropIfExists('brands');
    }
}
