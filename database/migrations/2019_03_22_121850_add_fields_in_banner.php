<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFieldsInBanner extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('banners', function (Blueprint $table) {
            $table->integer('cost');
            $table->string('link');
            $table->integer('brand_id')->unsigned();
            $table->integer('product_type_id')->unsigned();

            $table->foreign('brand_id')->references('id')->on('brands');
            $table->foreign('product_type_id')->references('id')->on('product_types');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('banners', function($table)
        {
            $table->dropForeign('banners_brand_id_foreign');
            $table->dropForeign('banners_product_type_id_foreign');
        });

        Schema::table('banners', function (Blueprint $table) {
            $table->dropColumn('cost');
            $table->dropColumn('link');
            $table->dropColumn('brand_id');
            $table->dropColumn('product_type_id');
        });
    }
}
