class Fetch {
    sendPost(url, data, callback) {
        fetch(url, {
            headers: {
                "Content-Type": "application/json",
                "Accept": "application/json, text-plain, */*",
                "X-Requested-With": "XMLHttpRequest",
                "X-CSRF-TOKEN": document.querySelector('input[name="_token"]').value
            },
            method: 'post',
            credentials: "same-origin",
            body: JSON.stringify(data)
        }).then(function (response) {
            return response.json();
        }).then(function (json) {
            callback(json);
        }).catch(function (error) {
            console.log(error);
        });
    };

    sendPhoto(url, data, callback) {
        fetch(url, {
            headers: {
                "X-CSRF-TOKEN": document.querySelector('input[name="_token"]').value
            },
            method: 'POST',
            body: data
        }).then(function (response) {
            return response.json();
        }).then(function (json) {
            callback(json);
        }).catch(function (error) {
            console.log(error);
        });
    }
}