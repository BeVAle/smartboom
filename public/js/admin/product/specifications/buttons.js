$(document).on("click", '.create-group', function (event) {
    event.preventDefault();
    let groupId = findLastGroupId();
    (new Fetch).sendPost('/product-specs/add-group', {'groupId': groupId}, function (data) {
        if (data.success) {
            $('.blocks-group').append(data.view);
            $('.group-select' + (groupId + 1)).select2({
                tags: true,
                dropdownParent: $(".group").last().find('.group-name-wrap .form-group'),
                createTag: function (tag) {
                    return {
                        id: tag.term,
                        text: tag.term,
                        isNew: true
                    };
                }
            }).on("select2:select", function (e) {
                if (e.params.data.isNew) {
                    let _this = $(this);
                    (new Fetch).sendPost('/product-specs/add-new-element', {
                        'groupTitle': e.params.data.text,
                        'type': 'groupName'
                    }, function (data) {
                        if (data.success) {
                            _this.find('[value="' + e.params.data.id + '"]').replaceWith('<option selected value="' + data.newId + '">' + e.params.data.text + '</option>');
                        } else {
                            _this.find('[value="' + e.params.data.id + '"]').replaceWith('');
                        }
                    });
                }
                $(this).closest('.group').find('.rows').html('');
            });
        }
    });
});

$(document).on("click", '.create-row', function (event) {
    event.preventDefault();
    let _thisGroup = $(this).closest('.group');
    let groupId = _thisGroup.data('group-id');
    let rowId;
    if (!( rowId = _thisGroup.find('.row-wrap').last().data('row-id'))){
        rowId = 0;
    }
    let charGroupNameId = $('.group-select' + groupId).val();
    (new Fetch).sendPost('/product-specs/add-row', {
        'groupId': groupId,
        'rowId': rowId,
        'charGroupNameId': charGroupNameId,
    }, function (data) {
        if (data.success) {
            _thisGroup.find('.rows').append(data.view);
            let newRowId = ++rowId;
            let lastRow = _thisGroup.find('.row-wrap').last();
            $('.group-' + groupId + '-row-key-' + newRowId).select2({
                tags: true,
                dropdownParent: lastRow.find('.key'),
                createTag: function (tag) {
                    return {
                        id: tag.term,
                        text: tag.term,
                        isNew: true
                    };
                }
            }).on("select2:select", function (e) {
                let _this = $(this);
                if (_this.val() == -1){
                    _this.closest('.row-wrap').find('.val').hide();
                    dropValueSelect(_this, groupId, newRowId);
                    return false;
                }
                if (e.params.data.isNew) {
                    (new Fetch).sendPost('/product-specs/add-new-element', {
                        'nameTitle': e.params.data.text,
                        'type': 'name',
                        'groupId': $('.group-select' + groupId).val()
                    }, function (data) {
                        let characteristicValue = $('.group-' + groupId + '-row-val-' + newRowId);
                        if (data.success) {
                            _this.find('[value="' + e.params.data.id + '"]').replaceWith('<option selected value="' + data.newId + '">' + e.params.data.text + '</option>');
                            characteristicValue.html('');
                            dropValueSelect(_this, groupId, newRowId);
                            return true;
                        } else {
                            _this.find('[value="' + e.params.data.id + '"]').replaceWith('');
                            characteristicValue.html('');
                            return true;
                        }
                    });
                }
                dropValueSelect(_this, groupId, newRowId);
                _this.closest('.row-wrap').find('.val').show();
            });


            $('.group-' + groupId + '-row-val-' + newRowId).select2({
                tags: true,
                dropdownParent: lastRow.find('.val'),
                createTag: function (tag) {
                    return {
                        id: tag.term,
                        text: tag.term,
                        isNew: true
                    };
                }
            }).on("select2:select", function (e) {
                if (e.params.data.isNew) {
                    let _this = $(this);
                    (new Fetch).sendPost('/product-specs/add-new-element', {
                        'valueTitle': e.params.data.text,
                        'type': 'value',
                        'nameId': $('.group-' + groupId + '-row-key-' + newRowId).val(),
                    }, function (data) {
                        if (data.success) {
                            _this.find('[value="' + e.params.data.id + '"]').replaceWith('<option selected value="' + data.newId + '">' + e.params.data.text + '</option>');
                        } else {
                            _this.find('[value="' + e.params.data.id + '"]').replaceWith('');
                        }
                    });
                }
            });

        }
    });
});

$(document).on("click", '.remove-group', function (event) {
    event.preventDefault();
    $(this).closest('.group').remove();
});

$(document).on("click", '.remove-row', function (event) {
    event.preventDefault();
    $(this).parent().remove();
});

function findLastGroupId() {
    return $(".group").last().data('group-id');
}