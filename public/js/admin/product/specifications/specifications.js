if ($('.group').length) {
    $('.group').each(function () {
        $('.group-select' + $(this).data('group-id')).select2({
            tags: true,
            dropdownParent: $(".group").last().find('.group-name-wrap .form-group'),
            createTag: function (tag) {
                return {
                    id: tag.term,
                    text: tag.term,
                    isNew: true
                };
            }
        }).on("select2:select", function (e) {
            let _this = $(this);
            if (e.params.data.isNew) {
                (new Fetch).sendPost('/product-specs/add-new-element', {
                    'groupTitle': e.params.data.text,
                    'type': 'groupName'
                }, function (data) {
                    if (data.success) {
                        _this.find('[value="' + e.params.data.id + '"]').replaceWith('<option selected value="' + data.newId + '">' + e.params.data.text + '</option>');
                    } else {
                        _this.find('[value="' + e.params.data.id + '"]').replaceWith('');
                    }
                });
            }
            _this.closest('.group').find('.rows').html('');
        });
    });
}

if ($('.row-wrap').length) {
    $('.row-wrap').each(function () {
        let rowWrap = $(this);
        let groupId = rowWrap.closest('.group').data('group-id');
        let rowId = rowWrap.data('row-id');

        $('.group-' + groupId + '-row-key-' + rowId).select2({
            tags: true,
            dropdownParent: rowWrap.find('.key'),
            createTag: function (tag) {
                return {
                    id: tag.term,
                    text: tag.term,
                    isNew: true
                };
            }
        }).on("select2:select", function (e) {
            let _this = $(this);
            if (e.params.data.isNew) {
                (new Fetch).sendPost('/product-specs/add-new-element', {
                    'nameTitle': e.params.data.text,
                    'type': 'name',
                    'groupId': $('.group-select' + groupId).val()
                }, function (data) {
                    let characteristicValue = $('.group-' + groupId + '-row-val-' + rowId);
                    if (data.success) {
                        _this.find('[value="' + e.params.data.id + '"]').replaceWith('<option selected value="' + data.newId + '">' + e.params.data.text + '</option>');
                        characteristicValue.html('');
                        dropValueSelect(_this, groupId, rowId);
                        return true;
                    } else {
                        _this.find('[value="' + e.params.data.id + '"]').replaceWith('');
                        characteristicValue.html('');
                        return true;
                    }
                });
            }
            dropValueSelect(_this, groupId, rowId);
        });

        $('.group-' + groupId + '-row-val-' + rowId).select2({
            tags: true,
            dropdownParent: rowWrap.find('.val'),
            createTag: function (tag) {
                return {
                    id: tag.term,
                    text: tag.term,
                    isNew: true
                };
            }
        }).on("select2:select", function (e) {
            if (e.params.data.isNew) {
                let _this = $(this);
                (new Fetch).sendPost('/product-specs/add-new-element', {
                    'valueTitle': e.params.data.text,
                    'type': 'value',
                    'nameId': $('.group-' + groupId + '-row-key-' + rowId).val(),
                }, function (data) {
                    if (data.success) {
                        _this.find('[value="' + e.params.data.id + '"]').replaceWith('<option selected value="' + data.newId + '">' + e.params.data.text + '</option>');
                    } else {
                        _this.find('[value="' + e.params.data.id + '"]').replaceWith('');
                    }
                });
            }
        });
    })
}

function dropValueSelect(_this, groupId, newRowId) {
    let characteristicValue = $('.group-' + groupId + '-row-val-' + newRowId);
    characteristicValue.html('');
    if (isNaN(_this.val())) {
        return false;
    }
    (new Fetch).sendPost('/product-specs/add-value', {'charNameId': _this.val()}, function (data) {
        if (data.success) {
            $.each(data.characteristicValue, function (index, value) {
                characteristicValue.append('<option selected value="' + index + '">' + value + '</option>')
            });
        }
    });
}