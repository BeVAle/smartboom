if (document.querySelector('.gallery-uploader')) {
    document.querySelectorAll('.gallery-uploader').forEach(function (element) {
        setDeleteItemGallery();
        element.addEventListener('change', function (event) {
            let main = document.querySelector('.main');
            Array.prototype.forEach.call(element.files, function (galleryElement) {
                let files = new FormData();
                files.append('photo', galleryElement);
                (new Fetch).sendPhoto('/tool/upload', files, function (data) {
                    if (data.status === 'success') {
                        main.innerHTML += ('<div class="js-gallery-item">\n' +
                            '<div class="img-wrap"><img src="' + data.filePath + '"/><input type="text" class="hide filename" name="gallery[]" value="' + data.filePath + '"></div>' +
                            '<div class="text-wrap"><button>Удалить</button></div>' +
                            '</div>');
                        setDeleteItemGallery();
                    }
                });
            })
        });
    });
}

function setDeleteItemGallery() {
    if (document.querySelector('.text-wrap button')) {
        document.querySelectorAll('.text-wrap button').forEach(function (element) {
            element.addEventListener('click', function (event) {
                event.preventDefault();
                let target = element;
                while (!target.classList.contains('main')) {
                    if (target.classList.contains('js-gallery-item')) {
                        target.parentNode.removeChild(target);
                        return;
                    }
                    target = target.parentNode;
                }
            });
        });
    }
}


