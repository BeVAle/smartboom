function initCropper(image,aspectRatio,cropperBlock) {
    (new Cropper(image)).destroy();
    const croper = new Cropper(image, {
        aspectRatio: aspectRatio,
        zoomable: false,
        checkImageOrigin: false,
        cropend: function () {
            var data = croper.getData();
            cropperBlock.querySelector('#cropX').value = data.x;
            cropperBlock.querySelector('#cropY').value = data.y;
            cropperBlock.querySelector('#cropWidth').value = data.width;
            cropperBlock.querySelector('#cropHeight').value = data.height;
        },
    });
    var data = croper.getData();
    cropperBlock.querySelector('#cropX').value = data.x;
    cropperBlock.querySelector('#cropY').value = data.y;
    cropperBlock.querySelector('#cropWidth').value = data.width;
    cropperBlock.querySelector('#cropHeight').value = data.height;
}