if (document.querySelector('.js-image-uploader')) {
    document.querySelectorAll('.js-image-uploader').forEach(function (element) {
        element.imageUploader();
    });
}


if (document.querySelector('.upload-photo-input')) {
    document.querySelectorAll('.upload-photo-input').forEach(function (element) {
        element.addEventListener('change', function (event) {
            let files = new FormData();
            files.append('photo', document.forms['form-uploader']['file'].files[0]);
            (new Fetch).sendPhoto('/tool/upload', files, function (data) {
                if (data.status === 'success') {
                    document.querySelector('.pre-review-photo').src = data.filePath;
                    document.querySelector('.link-holder').value = data.filePath;
                    const image = document.querySelector('.pre-review-photo');
                    initCropper(image, 0, document.querySelector('.cropper-block'));
                }
            });
        });
    });
}


if (document.querySelector('#ckeditor')){
    document.querySelectorAll('#ckeditor').forEach(function (element) {
        $('.kek').ckeditor();
        // CKEDITOR.replace( '#ckeditor' );
    });
}