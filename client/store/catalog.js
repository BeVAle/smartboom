import debounce from "lodash.debounce";

const state = () => ({
	WorthSeeing: [],
	products: [],
	categories: [],
	brandsInFooter: [],
	accessoriesInFooter: [],
	isLoadMore: true,
	count: 12,
	brands: [],
	sorting: {
		price: 1,
		new: 0
	},
	filters: {
		category: 1,
		brands: [],
		only_new: false,
		rangePrice: {
			from: {
				placeholder: 0,
				val: ""
			},
			to: {
				placeholder: 150000,
				val: ""
			}
		}
	}
});

const getters = {
	checkCategory: state => id => {
		return state.filters.category == id;
	},
	checkBrand: state => id => {
		return state.filters.brands.includes(id);
	}
};

const mutations = {
	getCategories(state, payload) {
		state.categories = payload;
		let accessories = payload
			.filter(item => item.title === "Аксессуары")
			.map(item => ({
				id: item.id,
				title: item.title
			}));
		let brands = payload.map(item => item.brands);

		state.brandsInFooter = brands[0].filter(
			item =>
				item.title === "Apple" ||
				item.title === "Samsung" ||
				item.title === "Xiaomi"
		);
		state.accessoriesInFooter = accessories;
	},
	catalogProducts(state, payload) {
		state.products = payload.products;
		state.isLoadMore = !payload.isLoadMore;
		state.count = payload.count;
	},
	getWorthSeeing(state, payload) {
		state.WorthSeeing = payload;
	},
	changeSorting(state, payload) {
		if (payload.name === "По цене") {
			state.sorting.price = !state.sorting.price;
		}
		if (payload.name === "По новизне") {
			state.sorting.new = !state.sorting.new;
			state.only_new = !state.only_new;
		}
	},
	toggleBrand(state, payload) {
		state.filters.category = payload.categoryId;
		if (state.filters.brands.includes(payload.brandId)) {
			state.filters.brands.splice(
				state.filters.brands.indexOf(payload.brandId),
				1
			);
		} else {
			state.filters.brands.push(payload.brandId);
		}
	},
	filterByNew(state) {
		state.only_new = true;
	},
	activeCategory(state, payload) {
		state.only_new = false;
		state.filters.category = payload;
		state.isLoadMore = true;
		state.count = 12;
		state.filters.brands = [];
	},
	rangePriceFromChange(state, payload) {
		if (payload.data == "e") {
			state.filters.rangePrice.from.val = state.filters.rangePrice.from.val;
		} else {
			state.filters.rangePrice.from.val = payload.target.value;
		}
	},
	rangePriceToChange(state, payload) {
		state.filters.rangePrice.to.val = payload.target.value;
	},
	resetFilters(state) {
		state.filters = {
			category: 1,
			brands: [],
			only_new: false,
			rangePrice: {
				from: {
					placeholder: 0,
					val: ""
				},
				to: {
					placeholder: 150000,
					val: ""
				}
			}
		};
	}
};
const actions = {
	async initCatalog({ dispatch }, payload) {
		await dispatch("getCategories");
		await dispatch("applyFilter", payload);
		await dispatch("getProduct");
	},
	async getCategories({ commit }) {
		const { data } = await this.$axios.post("/product-type");
		commit("getCategories", data.productTypes);
	},
	async getWorthSeeing({ commit }) {
		const { data } = await this.$axios.post("/product", { hit: 1, count: 4 });
		data.products.forEach(el => {
			el.count = 1;
		});
		commit("getWorthSeeing", data.products);
	},
	async getProduct({ commit, state }, payload) {
		let offset;
		let _count = payload ? payload + state.count : state.count;

		const { data } = await this.$axios.post("/product", {
			category: state.filters.category,
			price: {
				from: state.filters.rangePrice.from.val,
				to: state.filters.rangePrice.to.val
			},
			count: _count,
			brands: state.filters.brands,
			offset: offset,
			only_new: state.only_new || null,
			sort: state.sorting
		});

		data.products.forEach(el => {
			el.count = 1;
		});

		commit("catalogProducts", {
			products: data.products,
			count: _count,
			isLoadMore: data.products.length < _count
		});
	},
	applyFilter: debounce(({ dispatch }) => {
		dispatch("getProduct");
	}, 300),

	changeSorting({ commit }, payload) {
		commit("changeSorting", payload);
	},
	activeCategory({ commit }, payload) {
		commit("activeCategory", payload);
	},
	filterByNew({ commit }, payload) {
		commit("filterByNew", payload);
	},
	toggleBrand({ commit }, payload) {
		commit("toggleBrand", payload);
	},
	rangePriceFromChange({ commit }, payload) {
		commit("rangePriceFromChange", payload);
	},
	rangePriceToChange({ commit }, payload) {
		commit("rangePriceToChange", payload);
	},
	resetFilters({ commit }) {
		commit("resetFilters");
	}
};

export default {
	state,
	getters,
	mutations,
	actions
};
