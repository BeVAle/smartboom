const state = () => ({
	cartProducts: [],
	discountPercent: 10,
	promoCodeName: "",
	promoCodePercent: 0,
	freeDeliveryLimit: 300000,
	isDelivery: false,
	delivery: 0,
	pickup: 0,
	defaultDeliveryPrice: 390,
	deliveryInRegion: 890,
	deliveryExpress: 890
});

const getters = {
	cartProductsCount: state => {
		return state.cartProducts.length;
	},
	orderPrice: state => {
		let price = 0;
		state.cartProducts.forEach(element => {
			let _count = element.count || 1;
			price = price + element.price * _count;
		});
		return price;
	},
	discountFromPromoCode: (state, getters) => {
		let discount = 0;
		discount = (getters.orderPrice / 100) * state.promoCodePercent;
		return discount;
	},
	discount: (state, getters) => {
		let discount = 0;
		discount = (getters.orderPrice / 100) * state.discountPercent;
		return discount;
	},
	deliveryPrice: (state, getters) => {
		// if (getters.orderPrice > state.freeDeliveryLimit) {
		// 	return 0;
		// }
		if (state.isDelivery === "delivery") {
			return state.defaultDeliveryPrice;
		}
		if (state.isDelivery === "deliveryInRegion") {
			return state.deliveryInRegion;
		}
		if (state.isDelivery === "deliveryExpress") {
			return state.deliveryExpress;
		}

		if (state.isDelivery === "pickup") {
			return 0;
		}

		return 0;
	},
	totalPrice: (state, getters) => {
		// if (state.isDelivery === "delivery") {
		// 	return getters.orderPrice + state.defaultDeliveryPrice;
		// }
		// if (state.isDelivery === "deliveryExpress") {
		// 	return getters.orderPrice + state.deliveryExpress;
		// }
		// if (state.isDelivery === "deliveryInRegion") {
		// 	return getters.orderPrice + state.deliveryInRegion;
		// } else {

		return parseInt(getters.orderPrice + getters.deliveryPrice);
		//	}
		// getters.discount -
		// getters.discountFromPromoCode +
	},
	checkInCart: state => id => {
		return state.cartProducts.map(item => item.id).includes(id);
	}
};

const mutations = {
	resetCart(state) {
		state.cartProducts = [];
	},
	addToCart(state, payload) {
		payload.count = payload.count || 1;
		state.cartProducts.push(payload);
	},
	decrement(state, payload) {
		payload.count--;
	},
	increment(state, payload) {
		payload.count++;
	},
	remove(state, payload) {
		payload.count = 1;
		state.cartProducts.splice(state.cartProducts.indexOf(payload), 1);
	},
	addedPromoCode(state, payload) {
		state.promoCodeName = payload.name;
		state.promoCodePercent = payload.val;
	},
	toggleDelivery(state, payload) {
		if (payload === "delivery") {
			state.delivery = state.defaultDeliveryPrice;
		} else {
			state.delivery = state[payload];
		}
		state.isDelivery = payload;
	}
};
const actions = {
	resetCart({ commit }) {
		commit("resetCart");
	},
	addToCart({ commit }, item) {
		commit("addToCart", item);
	},
	decrement({ commit }, payload) {
		if (payload.count > 1) {
			commit("decrement", payload);
		} else if ((payload.count = 1)) {
			commit("remove", payload);
		}
	},
	increment({ commit }, payload) {
		commit("increment", payload);
	},
	remove({ commit }, payload) {
		commit("remove", payload);
	},
	async addedPromoCode({ commit }, payload) {
		// const { data } = await this.$axios.post("/product", payload);
		let todo = {
			name: payload,
			val: 20
		};
		commit("addedPromoCode", todo);
	},
	toggleDelivery({ commit }, payload) {
		commit("toggleDelivery", payload);
	}
};

export default {
	state,
	getters,
	mutations,
	actions
};
