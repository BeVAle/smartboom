const state = () => ({
  product: {},
  activeGalleryImg: "",
});

const mutations = {
  getProduct(state, payload) {
    state.product = payload;
  },
  setProductGallery(state) {
    state.product.photos = state.product.photos.map((imageSrc, index) => {
      index === 0 ? (state.activeGalleryImg = imageSrc) : false;
      return {
        src: imageSrc,
        id: index,
        active: index == 0,
      };
    });
  },
  changeActiveGalleryImg(state, item) {
    state.activeGalleryImg = item.src;
    state.product.photos.forEach(element => {
      element.active = false;
    });
    item.active = true;
  },
};
const actions = {
  async getProduct({ commit }, payload) {
    const { data } = await this.$axios.post(`/product/view/${payload.id}`);
    commit("getProduct", data.product);
    if (data.product.photos.length) {
      commit("setProductGallery");
    }
  },
  changeActiveGalleryImg({ commit }, item) {
    commit("changeActiveGalleryImg", item);
  },
};

export default {
  state,
  mutations,
  actions,
};
