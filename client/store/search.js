import debounce from "lodash.debounce";

const state = () => ({
	resultSearch: []
});

const mutations = {
	search(state, payload) {
		const products = payload.filter(item => item.type === "Product");
		const productsTypes = payload.filter(item => item.type === "Product Type");

		state.resultSearch = [
			{
				name: "Товары",
				items: products
			},
			{
				name: "Категории",
				items: productsTypes
			}
		];
	}
};

const actions = {
	search: debounce(async function({ commit }, payload) {
		if (payload) {
			const { data } = await this.$axios.post(`/elastic`, {
				query: payload
			});
			if (data.success) {
				commit("search", data.items);
			}
		}
	}, 400)
};

export default {
	state,
	mutations,
	actions
};
