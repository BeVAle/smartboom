const state = () => ({
	categories: [],
	banners: []
});
const getters = {
	numberFormat: state => n => {
		return new Intl.NumberFormat("ru-RU", {
			style: "currency",
			currency: "RUB",
			minimumFractionDigits: 0
		}).format(n);
	}
};
const mutations = {
	getCategories(state, payload) {
		state.categories = payload;
	},
	catalogProducts(state, payload) {
		state.products = payload;
	},
	addBanners(state, payload) {
		state.banners = payload;
	}
};
const actions = {
	async getData({ dispatch }) {
		dispatch("getCategories");
	},
	async getCategories({ commit }) {
		const { data } = await this.$axios.post("/product-type");
		commit("getCategories", data.productTypes);
	},
	async getBanners({ commit }) {
		const { data } = await this.$axios.post("/banner");
		commit("addBanners", data.brands);
	}
};

export default {
	state,
	getters,
	mutations,
	actions
};
