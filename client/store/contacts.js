export const state = () => ({
	contacts: {
		tel1: {
			title: "Служба доставки",
			info: "8 (901) 381-04-86",
			link: "tel:8 (901) 381-04-86"
		},
		tel2: {
			title: "Справочная",
			info: "8 (901) 381-04-86",
			link: "tel:8 (901) 381-04-86"
		},
		address: {
			title: "Адрес магазина",
			info: "г. Москва ул. Краснодарская дом 66 офис 1"
		},
		mail: {
			title: "Почта",
			info: "support@useit.store",
			link: "mailto:support@useit.store"
		},
		schedule: {
			title: "График работы",
			text: ["Пн-Пт: с 11:00 до 20:00", "Сб: с 12:00 до 18:00", "Вс: выходной"]
		}
	},
	map: {
		center: { lat: 55.67409256903493, lng: 37.77311549999993 },
		markers: [
			{
				position: { lat: 55.67409256903493, lng: 37.77311549999993 }
			}
		]
	},
	socialLinks: {
		vk: {
			name: "vk",
			url: "https://vk.com/useit.store"
		},
		instagram: {
			name: "instagram",
			url: "https://www.instagram.com/useit.store_mobile/?r=nametag"
		},
		whatsapp: {
			name: "whatsapp",
			url: "whatsapp://send?phone=+79013810486"
		},
		telegram: {
			name: "telegram",
			url: "tg://resolve?domain=useitstore"
		},
		avito: {
			name: "avito",
			url:
				"https://www.avito.ru/user/ed28fa64fd0fab306fe03aa00608d107/profile?src=sharing"
		},
		youla: {
			name: "youla",
			url:
				"https://youla.ru/user/5c1a21b2c15ae38e94084198?utm_source=profiles&utm_medium=backend"
		}
	}
});
