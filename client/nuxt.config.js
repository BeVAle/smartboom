const axios = require("axios");
const axiosApiUrl = "https://api.useit.store/api";
// process.env.NODE_ENV == "development"
// 	? "http://smartboom.gc/api"
// 	: "https://api.useit.store/api";

module.exports = {
	/*
	 ** Headers of the page
	 */
	head: {
		title: "Useit - Смартфоны и аксессуары",
		meta: [
			{ charset: "utf-8" },
			{
				name: "viewport",
				content: "width=device-width, initial-scale=1"
			},
			{
				name: "yandex-verification",
				content: "b6d8210eae65b7f3"
			},
			{
				property: "og:type",
				content: "article"
			},
			{
				property: "og:description",
				content: "Смартфоны и аксессуары"
			},
			{
				property: "og:title",
				content: "Useit - Смартфоны и аксессуары"
			},
			{
				property: "og:image",
				content: "/opengraph.png"
			},
			{
				hid: "description",
				name: "description",
				content: "Смартфоны и аксессуары"
			}
		],
		link: [{ rel: "icon", type: "image/x-icon", href: "/favicon.png" }]
	},
	/*
	 ** Customize the progress bar color
	 */
	loading: { color: "#2f2361" },

	css: [
		{
			src: "~assets/styles/main.scss",
			lang: "sass"
		}
	],

	modules: [
		[
			"@nuxtjs/google-tag-manager",
			{
				id: "GTM-MPBRFFJ",
				pageTracking: true
			}
		],
		["@nuxtjs/style-resources"],
		[
			"@nuxtjs/axios",
			{
				baseURL: axiosApiUrl,
				proxyHeaders: false,
				credentials: false
			}
		]
	],
	styleResources: {
		scss: ["~assets/styles/_global.scss"]
	},
	plugins: [
		"~plugins/vue-the-mask",
		"~plugins/vue-js-modal",
		// '~plugins/vue-datetime',
		// { src: '~plugins/vue-core-image-upload', ssr: false },
		{
			src: "~/plugins/vue2-google-maps.js",
			ssr: false
		},
		{
			src: "~/plugins/vue-carousel.js",
			ssr: false
		}
	],
	/*
	 ** Build configuration
	 */
	build: {
		vendor: ["vue2-google-maps", "vue-the-mask"],
		// vendor: ["axios"],
		/*
		 ** Run ESLint on save
		 */
		postcss: {
			"postcss-cssnext": {
				browsers: ["last 2 versions", "ie >= 9"]
			}
		},
		extend(config, { isDev, isClient }) {
			const svgRule = config.module.rules.find(rule => rule.test.test(".svg"));

			svgRule.test = /\.(png|jpe?g|gif|webp)$/;
			config.module.rules.push({
				test: /\.svg$/,
				loader: "vue-svg-loader"
			});

			if (isDev && isClient) {
				config.module.rules.push({
					enforce: "pre",
					test: /\.(js|vue)$/,
					loader: "eslint-loader",
					exclude: /(node_modules)/
				});
			}
		}
	}
};
