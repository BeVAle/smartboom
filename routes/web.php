<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


//Admin route
Auth::routes();

/* CRUD Controllers */
Route::resource('banner', 'Admin\Banner\BannerController');
Route::resource('brand', 'Admin\Brand\BrandController');
Route::resource('product', 'Admin\Product\ProductController');
Route::group(['prefix' => 'product-specs/'], function () {
    Route::post('/add-group','Admin\Product\ProductSpecificationsController@addGroup');
    Route::post('/add-row','Admin\Product\ProductSpecificationsController@addRow');
    Route::post('/add-new-element','Admin\Product\ProductSpecificationsController@addNewElement');
    Route::post('/add-value','Admin\Product\ProductSpecificationsController@addValue');
});
Route::resource('product-type', 'Admin\ProductType\ProductTypeController');
/* End */


Route::resource('site', 'Admin\SiteController');
Route::post('/tool/upload','Tool\UploadController@upload');
Route::get('/home', 'HomeController@index');
Route::get('/', 'HomeController@index');
Route::resource('products', 'ProductsController');
