<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

//Route::middleware('auth:api')->get('/user', function (Request $request) {
//    return $request->user();
//});

Route::get('/hit', 'MainController@index');

Route::group(['prefix' => 'product/'], function () {
    Route::post('/', 'Product\ProductController@index');
    Route::post('/view/{id}', 'Product\ProductController@view');
});

Route::group(['prefix' => 'brand/'], function () {
    Route::post('/','Brand\BrandController@index');
});

Route::group(['prefix' => 'banner/'], function () {
    Route::post('/','Banner\BannerController@index');
});


Route::group(['prefix' => 'product-type/'], function () {
    Route::post('/','ProductType\ProductTypeController@index');
});

Route::group(['prefix' => 'notification/'], function () {
    Route::post('/help','Notification\NotificationController@sendHelp');
    Route::post('/order','Notification\NotificationController@sendOrder');
    Route::post('/callback','Notification\NotificationController@sendCallback');
});

Route::group(['prefix' => 'elastic/'], function () {
    Route::post('/','Elastic\ElasticController@get');
});
