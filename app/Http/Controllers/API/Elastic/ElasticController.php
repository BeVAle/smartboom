<?php
/**
 * Created by PhpStorm.
 * User: BeVAle
 * Date: 20.03.2019
 * Time: 12:56
 */

namespace App\Http\Controllers\API\Elastic;


use App\Http\Requests\API\Elastic\ElasticRequest;
use App\Presenters\Api\Elastic\ElasticPresenter;
use App\Services\ElasticService;

class ElasticController
{
    public function get(ElasticRequest $request)
    {
        $query = $request->all();
        $answer = (new ElasticService())->get($query['query']);
        return response()->json([
            'success' => true,
            'items' => (new ElasticPresenter($answer))->toArray(),
        ]);
    }
}