<?php
/**
 * Created by PhpStorm.
 * User: BeVAle
 * Date: 15.02.2019
 * Time: 19:24
 */

namespace App\Http\Controllers\API\Brand;


use App\Presenters\Api\Brand\BrandPresenter;

class BrandController
{
    public function index()
    {
        return (new BrandPresenter())->toJson();
    }
}