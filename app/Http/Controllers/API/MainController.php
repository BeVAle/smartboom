<?php
/**
 * Created by PhpStorm.
 * User: BeVAle
 * Date: 12.02.2019
 * Time: 18:25
 */

namespace App\Http\Controllers\API;

use App\Entity\Product\Models\Product;
use Illuminate\Routing\Controller as BaseController;

class MainController extends BaseController
{
    public function index()
    {
        return response()->json([
            'status' => 'success',
            'products' => Product::where('hit','1')->get()->all()
        ]);
    }
}