<?php
/**
 * Created by PhpStorm.
 * User: BeVAle
 * Date: 15.02.2019
 * Time: 19:55
 */

namespace App\Http\Controllers\API\ProductType;


use App\DTO\API\ProductTypeFilterDTO;
use App\Presenters\Api\ProductType\ProductTypePresenter;
use Illuminate\Http\Request;

class ProductTypeController
{
    public function index(Request $request)
    {
        return (new ProductTypePresenter((new ProductTypeFilterDTO($request))))->toJson();
    }
}