<?php
/**
 * Created by PhpStorm.
 * User: BeVAle
 * Date: 12.02.2019
 * Time: 18:17
 */

namespace App\Http\Controllers\API\Product;


use App\DTO\API\ProductFilterDTO;
use App\Entity\Product\Repositories\ProductRepository;
use App\Presenters\Api\Product\ProductPresenter;
use Illuminate\Http\Request;

class ProductController
{
    /**
     * @var ProductRepository
     */
    private $productRepository;
    /**
     * @var ProductPresenter
     */
    private $productAbstractPresenter;

    /**
     * ProductController constructor.
     * @param ProductRepository $productRepository
     * @param ProductPresenter $productAbstractPresenter
     */
    public function __construct(ProductRepository $productRepository, ProductPresenter $productAbstractPresenter)
    {
        $this->productRepository = $productRepository;
        $this->productAbstractPresenter = $productAbstractPresenter;
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(Request $request)
    {
        $presenter = $this->productAbstractPresenter;
        return response()->json([
            'success' => true,
            'products' => collect($this->productRepository->filter((new ProductFilterDTO($request))))->map(function ($product) use ($presenter) {
                return $presenter->toArray($product);
            }),
        ]);
    }

    /**
     * @param int $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function view(int $id)
    {
        return response()->json([
            'success' => true,
            'product' =>  $this->productAbstractPresenter->toArray($this->productRepository->findById($id)),
        ]);
    }
}