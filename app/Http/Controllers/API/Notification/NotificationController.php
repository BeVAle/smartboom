<?php
/**
 * Created by PhpStorm.
 * User: BeVAle
 * Date: 08.03.2019
 * Time: 13:47
 */

namespace App\Http\Controllers\API\Notification;

use App\Http\Requests\API\Notification\NotificationRequest;
use App\Http\Requests\API\Notification\OrderRequest;
use App\Mail\SendOrder;
use App\Mail\SendOrderToCustomer;
use App\Presenters\Api\Notifications\Order;
use Carbon\Carbon;
use Illuminate\Support\Facades\Mail;
use App\Mail\SendHelp;

class NotificationController
{
    public function sendHelp(NotificationRequest $request)
    {
        $credentials = $request->only('name', 'phone');
        $validator = \Validator::make($credentials, [
            'name' => 'required',
            'phone' => 'required',
        ]);
        if ($validator->fails()) {
            return response()
                ->json(['errors' => $validator->errors()], 422);
        }
        $data = [
            'title' => 'Помощь',
            'name' => $request->name,
            'phone' => $request->phone,
            'problem' => $request->problem,
            'orderNumber' => $request->orderNumber,
        ];
        Mail::to('vb@goldcarrot.ru')->send(new SendHelp($data));
        Mail::to('support@useit.store')->send(new SendHelp($data));
        return response()->json(['success' => true]);
    }

    public function sendOrder(OrderRequest $request)
    {

        $validator = \Validator::make($request->all(), [
            'name' => 'required|string',
            'phone' => 'required|string',
            'products' => 'required|array',
            'email' => 'required|string',
            'city' => 'sometimes|nullable|string',
            'street' => 'sometimes|nullable|string',
            'houseNumber' => 'sometimes|nullable|string',
            'typeDelivery' => 'sometimes|nullable|integer',
            'typePayment' => 'sometimes|nullable|integer',
            'promoCode' => 'sometimes|nullable|string',
            'cost' => 'sometimes|nullable|numeric',
            'comments' => 'sometimes|nullable|string',
            'apartment' => 'sometimes|nullable|integer',
        ]);
        if ($validator->fails()) {
            return response()
                ->json(['errors' => $validator->errors()], 422);
        }
        $timestamp = Carbon::now()->timestamp;
        try {
            $order = new \App\Entity\Order\Models\Order([
                'name' => $request->name,
                'phone' => $request->phone,
                'email' => $request->email,
                'city' => $request->city,
                'street' => $request->street,
                'houseNumber' => $request->houseNumber,
                'typeDelivery' => $request->typeDelivery,
                'typePayment' => $request->typePayment,
                'promoCode' => $request->promoCode,
                'cost' => $request->cost,
                'comments' => $request->comments,
                'apartment' => $request->apartment,
            ]);
            $order->save();
        } catch (\Exception $e) {
            // do task when error
            var_dump ($e->getMessage());
            exit();// insert query
        }
        switch ($request->typeDelivery) {
            case 1:
                $returnTypeDelivery = 'Доставка курьером';
                break;
            case 2:
                $returnTypeDelivery = 'Самовывоз';
                break;
            case 3:
                $returnTypeDelivery = 'Доставка курьером по МО';
                break;
            case 4:
                $returnTypeDelivery = 'Экспресс доставка курьером';
                break;
            default:
                $returnTypeDelivery = 'Самовывоз';
        }
        $data = [
            'name' => $request->name,
            'phone' => $request->phone,
            'email' => $request->email,
            'city' => $request->city,
            'street' => $request->street,
            'houseNumber' => $request->houseNumber,
            'typeDelivery' => $returnTypeDelivery,
            'typePayment' => $request->typePayment == 1 ? 'Карта' : 'Наличные',
            'promoCode' => $request->promoCode,
            'cost' => $request->cost,
            'comments' => $request->comments,
            'apartment' => $request->apartment,
            'products' => (new Order)->toArray($request->products),
            'order_number' => $order->id + 5000,
        ];
        Mail::to($request->email)->send(new SendOrderToCustomer($data));
        Mail::to('vb@goldcarrot.ru')->send(new SendOrder($data));
        Mail::to('ar@goldcarrot.ru')->send(new SendOrder($data));
        Mail::to('zakaz@useit.store')->send(new SendOrder($data));
        return response()->json(['success' => true, 'timestamp' =>  $order->id + 5000]);
    }


    public function sendCallback(NotificationRequest $request)
    {
        $credentials = $request->only('name', 'phone');
        $validator = \Validator::make($credentials, [
            'name' => 'required',
            'phone' => 'required',
        ]);
        if ($validator->fails()) {
            return response()
                ->json(['errors' => $validator->errors()], 422);
        }
        $data = [
            'title' => 'Закажите звонок',
            'name' => $request->name,
            'phone' => $request->phone,
            'problem' => $request->problem,
            'orderNumber' => $request->orderNumber,
        ];
        Mail::to('vb@goldcarrot.ru')->send(new SendHelp($data));
        Mail::to('support@useit.store')->send(new SendHelp($data));
        return response()->json(['success' => true]);
    }
}