<?php
/**
 * Created by PhpStorm.
 * User: BeVAle
 * Date: 21.03.2019
 * Time: 13:58
 */

namespace App\Http\Controllers\API\Banner;


use App\Presenters\Api\Banner\BannerPresenter;

class BannerController
{
    public function index()
    {
        return (new BannerPresenter())->toJson();
    }
}