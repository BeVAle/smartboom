<?php
/**
 * Created by PhpStorm.
 * User: BeVAle
 * Date: 05.02.2019
 * Time: 13:06
 */

namespace App\Http\Controllers\Admin;


use Illuminate\Routing\Controller as BaseController;

class SiteController extends BaseController
{
    /**
     * Display a listing of the resource.
     *
     */
    public function index()
    {
        return view('admin/site.index');
    }


    public function show($id)
    {

    }

    public function login()
    {

    }

    public function logout()
    {

    }
}