<?php
/**
 * Created by PhpStorm.
 * User: BeVAle
 * Date: 21.03.2019
 * Time: 13:47
 */

namespace App\Http\Controllers\Admin\Banner;


use App\DTO\Admin\Banner\BannerCreateDTO;
use App\Entity\Banner\Repositories\BannerRepository;
use App\Entity\Banner\Services\BannerService;
use App\Http\Requests\Admin\Banner\BannerRequest;
use App\Presenters\Admin\Banner\BannerIndexPresenter;
use App\Presenters\Admin\Banner\BannerManagePresenter;
use App\Presenters\Admin\Banner\BannerViewPresenter;
use Illuminate\Routing\Controller as BaseController;

/**
 * @property BannerRepository $bannerRepository
 * @property BannerService $bannerService
 */
class BannerController  extends BaseController
{
    private $bannerRepository;
    private $bannerService;

    /**
     * BannerController constructor.
     * @param BannerRepository $bannerRepository
     * @param BannerService $bannerService
     */
    public function __construct(BannerRepository $bannerRepository, BannerService $bannerService)
    {
        $this->bannerRepository = $bannerRepository;
        $this->bannerService = $bannerService;
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        return view('admin/banner.index', [
            'presenter' => (new BannerIndexPresenter()),
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        return view('admin/banner.create',[
            'presenter' => (new BannerManagePresenter())
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param BannerRequest $request
     * @return \Illuminate\Http\Response
     */
    public function store(BannerRequest $request)
    {
        $this->bannerService->save(new BannerCreateDTO($request->validated()));
        return redirect('/banner')->with('success', 'Stock has been added');
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return Response
     */
    public function show(int $id)
    {
        return view('admin/banner.show', [
            'presenter' => (new BannerViewPresenter($id)),
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return Response
     */
    public function edit(int $id)
    {
        return view('admin/banner.edit', [
            'presenter' => (new BannerManagePresenter($id)),
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param BannerRequest $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(BannerRequest $request, int $id)
    {
        $this->bannerService->update(new BannerCreateDTO($request->validated()),$id);
        return redirect('/banner')->with('success', 'Stock has been added');
    }

    /**
     *  Remove the specified resource from storage.
     * @param int $id
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Exception
     */
    public function destroy(int $id)
    {
        $this->bannerService->removeById($id);
        return redirect('/banner')->with('success', 'Stock has been deleted Successfully');
    }
}