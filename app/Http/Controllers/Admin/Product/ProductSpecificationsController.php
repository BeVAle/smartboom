<?php
/**
 * Created by PhpStorm.
 * User: BeVAle
 * Date: 21.02.2019
 * Time: 16:40
 */

namespace App\Http\Controllers\Admin\Product;


use App\DTO\API\ProductSpecificationsDTO;
use App\Entity\Characteristic\Repositories\CharacteristicGroupNameRepository;
use App\Entity\Characteristic\Repositories\CharacteristicNameRepository;
use App\Entity\Characteristic\Repositories\CharacteristicValueRepository;
use App\Entity\Characteristic\Services\CharacteristicService;
use App\Http\Requests\API\Product\ProductSpecificationRequest;
use Illuminate\Routing\Controller as BaseController;

/**
 * @property CharacteristicService $characteristicService
 * @property CharacteristicGroupNameRepository $characteristicGroupNameRepository
 * @property CharacteristicNameRepository $characteristicNameRepository
 * @property CharacteristicValueRepository $characteristicValueRepository
 */
class ProductSpecificationsController extends BaseController
{
    private $characteristicGroupNameRepository;
    private $characteristicNameRepository;
    private $characteristicValueRepository;
    private $characteristicService;

    /**
     * ProductSpecificationsController constructor.
     * @param CharacteristicGroupNameRepository $characteristicGroupNameRepository
     * @param CharacteristicNameRepository $characteristicNameRepository
     * @param CharacteristicValueRepository $characteristicValueRepository
     * @param CharacteristicService $characteristicService
     */
    public function __construct(
        CharacteristicGroupNameRepository $characteristicGroupNameRepository,
        CharacteristicNameRepository $characteristicNameRepository,
        CharacteristicValueRepository $characteristicValueRepository,
        CharacteristicService $characteristicService)
    {
        $this->characteristicGroupNameRepository = $characteristicGroupNameRepository;
        $this->characteristicNameRepository = $characteristicNameRepository;
        $this->characteristicValueRepository = $characteristicValueRepository;
        $this->characteristicService = $characteristicService;
    }

    /**
     * @param ProductSpecificationRequest $request
     * @return \Illuminate\Http\JsonResponse
     * @throws \Throwable
     */
    public function addGroup(ProductSpecificationRequest $request)
    {
        $groupId = $request->post('groupId');
        return response()->json([
            'success' => true,
            'view' => view("admin/product/specification-fields.group", [
                'groupId' => ++$groupId,
                'characteristicGroupName' => $this->characteristicGroupNameRepository->findList(),
            ])->render()
        ]);
    }

    /**
     * @param ProductSpecificationRequest $request
     * @return \Illuminate\Http\JsonResponse
     * @throws \Throwable
     */
    public function addRow(ProductSpecificationRequest $request)
    {
        $groupId = $request->post('groupId');
        $rowId = $request->post('rowId');

        $collection = collect(['-1'=>'Выберите тип']);
        $nameListArr =  $this->characteristicNameRepository->findListByCharGroupName((int)$request->post('charGroupNameId'));
        $firstNameId = current(array_flip(current($nameListArr)));
        $nameListArr = $collection->concat($nameListArr);
        $nameValueArr = $this->characteristicValueRepository->findListByCharName($firstNameId);

        return response()->json([
            'success' => true,
            'view' => view("admin/product/specification-fields.row", [
                'groupId' => $groupId,
                'rowId' => ++$rowId,
                'characteristicName' => $nameListArr,
                'characteristicValue' =>$nameValueArr
            ])->render()
        ]);
    }

    /**
     * @param ProductSpecificationRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function addValue(ProductSpecificationRequest $request)
    {
        return response()->json([
            'success' => true,
            'characteristicValue' => $this->characteristicValueRepository->findListByCharName((int)$request->post('charNameId'))
        ]);
    }

    /**
     * @param ProductSpecificationRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function addNewElement(ProductSpecificationRequest $request)
    {
        if ($elem = $this->characteristicService->saveCharacteristicByTypeAndReturn(new ProductSpecificationsDTO($request))) {
            return response()->json([
                'success' => true,
                'newId' => $elem->id,
            ]);
        }
        return response()->json([
            'success' => false,
        ]);
    }
}