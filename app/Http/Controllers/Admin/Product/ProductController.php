<?php
/**
 * Created by PhpStorm.
 * User: BeVAle
 * Date: 07.02.2019
 * Time: 12:54
 */

namespace App\Http\Controllers\Admin\Product;


use App\DTO\Admin\Product\ProductDTO;
use App\Entity\Product\Models\Product;

use App\Entity\Product\Repositories\ProductRepository;
use App\Entity\Product\Services\ProductService;
use App\Http\Requests\Admin\Product\ProductRequest;
use App\Presenters\Admin\Product\ProductManagePresenter;
use App\Presenters\Admin\Product\ProductViewPresenter;
use Illuminate\Routing\Controller as BaseController;

class ProductController extends BaseController
{
    /** @var  ProductRepository */
    private $productRepository;
    private $productService;

    public function __construct(ProductRepository $productRepository, ProductService $productService)
    {
        $this->productRepository = $productRepository;
        $this->productService = $productService;
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $products = $this->productRepository->findAll();
        $productSearch = new Product();

        return view('admin/product.index', [
            'products' => $products,
            'productSearch' => $productSearch,
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        return view('admin/product.create', [
            'productPresenter' => (new ProductManagePresenter())
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param ProductRequest $request
     * @return \Illuminate\Http\Response
     */
    public function store(ProductRequest $request)
    {
        $this->productService->save(new ProductDTO($request->validated()));
        return redirect('/product')->with('success', 'Stock has been added');
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return Response
     */
    public function show($id)
    {
        return view('admin/product.show', [
            'productPresenter' => (new ProductViewPresenter($id)),
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return Response
     */
    public function edit(int $id)
    {
        return view('admin/product.edit', [
            'productPresenter' => (new ProductManagePresenter($id))
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param ProductRequest $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(ProductRequest $request, int $id)
    {
        $this->productService->update(new ProductDTO($request->validated()), $id);
        return redirect('/product')->with('success', 'Stock has been added');
    }


    /**
     * @param int $id
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Exception
     */
    public function destroy(int $id)
    {
        $this->productRepository->removeById($id);
        return redirect('/product')->with('success', 'Stock has been deleted Successfully');
    }
}