<?php
/**
 * Created by PhpStorm.
 * User: BeVAle
 * Date: 07.02.2019
 * Time: 15:42
 */

namespace App\Http\Controllers\Admin\ProductType;


use App\DTO\Admin\ProductType\ProductTypeDTO;
use App\Entity\ProductType\Repositories\ProductTypeRepository;
use App\Entity\ProductType\Services\ProductTypeService;
use App\Http\Requests\Admin\ProductType\ProductTypeRequest;
use Illuminate\Routing\Controller as BaseController;

/**
 * @property ProductTypeRepository $productTypeRepository
 * @property ProductTypeService $productTypeService
 */
class ProductTypeController extends BaseController
{
    private $productTypeRepository;
    private $productTypeService;

    /**
     * ProductTypeController constructor.
     * @param ProductTypeRepository $productRepository
     * @param ProductTypeService $productTypeService
     */
    public function __construct(ProductTypeRepository $productRepository, ProductTypeService $productTypeService)
    {
        $this->productTypeRepository = $productRepository;
        $this->productTypeService = $productTypeService;
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        return view('admin/product-type.index', [
            'productTypes' => $this->productTypeRepository->findAll()
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        return view('admin/product-type.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param ProductTypeRequest $request
     * @return \Illuminate\Http\Response
     */
    public function store(ProductTypeRequest $request)
    {
        $this->productTypeService->save(new ProductTypeDTO($request->validated()));
        return redirect('/product-type')->with('success', 'Stock has been added');
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return Response
     */
    public function show($id)
    {
        return view('admin/product-type.show', [
            'productType' => $this->productTypeRepository->findById($id)
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return Response
     */
    public function edit($id)
    {
        return view('admin/product-type.edit', [
            'productType' => $this->productTypeRepository->findById($id)
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param ProductTypeRequest $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(ProductTypeRequest $request, int $id)
    {
        $this->productTypeService->update(new ProductTypeDTO($request->validated()),$id);
        return redirect('/product-type')->with('success', 'Stock has been added');
    }

    /**
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Exception
     */
    public function destroy($id)
    {
        $this->productTypeRepository->removeById($id);
        return redirect('/product-type')->with('success', 'Stock has been deleted Successfully');
    }
}