<?php

namespace App\Http\Controllers\Admin\Brand;


use App\Entity\Brand\Repositories\BrandRepository;
use App\Entity\Brand\Services\BrandService;
use App\DTO\Admin\Brand\BrandCreateDTO;
use App\Http\Requests\Admin\Brand\BrandRequest;
use App\Presenters\Admin\Brand\BrandIndexPresenter;
use App\Presenters\Admin\Brand\BrandViewPresenter;
use Illuminate\Routing\Controller as BaseController;

/**
 * @property BrandRepository $brandRepository
 * @property BrandService $brandService
 */
class BrandController extends BaseController
{
    private $brandRepository;
    private $brandService;

    /**
     * BrandController constructor.
     * @param BrandRepository $brandRepository
     * @param BrandService $saveService
     */
    public function __construct(BrandRepository $brandRepository, BrandService $saveService)
    {
        $this->brandService = $saveService;
        $this->brandRepository = $brandRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        return view('admin/brand.index', [
            'presenter' => (new BrandIndexPresenter()),
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
//        dd(Request::getSchemeAndHttpHost());
        return view('admin/brand.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param BrandRequest $request
     * @return \Illuminate\Http\Response
     */
    public function store(BrandRequest $request)
    {
        $this->brandService->save(new BrandCreateDTO($request->validated()));
        return redirect('/brand')->with('success', 'Stock has been added');
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return Response
     */
    public function show(int $id)
    {
        return view('admin/brand.show', [
            'presenter' => (new BrandViewPresenter($id)),
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return Response
     */
    public function edit(int $id)
    {
        return view('admin/brand.edit', [
            'presenter' => (new BrandViewPresenter($id)),
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param BrandRequest $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(BrandRequest $request, int $id)
    {
        $this->brandService->update(new BrandCreateDTO($request->validated()),$id);
        return redirect('/brand')->with('success', 'Stock has been added');
    }

    /**
     *  Remove the specified resource from storage.
     * @param int $id
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Exception
     */
    public function destroy(int $id)
    {
        $this->brandService->removeById($id);
        return redirect('/brand')->with('success', 'Stock has been deleted Successfully');
    }
}
