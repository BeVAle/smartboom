<?php
/**
 * Created by PhpStorm.
 * User: BeVAle
 * Date: 05.02.2019
 * Time: 15:50
 */

namespace App\Http\Controllers\Tool;


use App\Services\UploadFileService;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller as BaseController;


class UploadController extends BaseController
{
    /**
     * @param Request $request
     * @return string
     */
    public function upload(Request $request)
    {
        if ($filePath = (new UploadFileService())->uploadImage($request)) {
            return response()->json([
                'status' => 'success',
                'filePath' => $filePath
            ]);
        }
        return response()->json([
            'status' => 'error',
            'filePath' => $filePath
        ]);

    }
}