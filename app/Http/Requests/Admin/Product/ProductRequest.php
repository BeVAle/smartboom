<?php

namespace App\Http\Requests\Admin\Product;

use Illuminate\Foundation\Http\FormRequest;

class ProductRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'brand_id' => 'required|integer',
            'product_type_id' => 'required|integer',
            'name' => 'required|string',
            'article' => 'required|string',
            'old_cost' => 'sometimes|nullable|integer',
            'cost' => 'integer',
            'color' => 'sometimes|nullable|string',
            'hit' => 'boolean',
            'new' => 'boolean',
            'description' => 'sometimes|nullable|string',
            'gallery' => 'array',
            'charGroup' => 'array',
        ];
    }
}
