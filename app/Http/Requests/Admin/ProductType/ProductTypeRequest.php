<?php
/**
 * Created by PhpStorm.
 * User: BeVAle
 * Date: 07.02.2019
 * Time: 15:50
 */

namespace App\Http\Requests\Admin\ProductType;


use Illuminate\Foundation\Http\FormRequest;

class ProductTypeRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title'=>'required',
            'logo'=> 'required|string',
            'popular' => 'boolean',
            'file' => 'image',
            'cropX' => 'numeric',
            'cropY' => 'numeric',
            'cropWidth' => 'numeric',
            'cropHeight' => 'numeric',

        ];
    }
}