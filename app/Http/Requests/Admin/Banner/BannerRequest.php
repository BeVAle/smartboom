<?php
/**
 * Created by PhpStorm.
 * User: BeVAle
 * Date: 21.03.2019
 * Time: 13:51
 */

namespace App\Http\Requests\Admin\Banner;


use Illuminate\Foundation\Http\FormRequest;

class BannerRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title' => 'required',
            'logo' => ['required', 'string'],
            'brand_id' => ['required','integer'],
            'product_type_id' => ['required','integer'],
            'cost' => 'integer',
            'link' => 'string',
            'desc' => 'string',
            'file' => 'image',
            'cropX' => 'numeric',
            'cropY' => 'numeric',
            'cropWidth' => 'numeric',
            'cropHeight' => 'numeric',
        ];
    }
}