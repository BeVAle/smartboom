<?php
/**
 * Created by PhpStorm.
 * User: BeVAle
 * Date: 28.02.2019
 * Time: 12:18
 */

namespace App\Http\Requests\API\Product;


use Illuminate\Foundation\Http\FormRequest;

class ProductSpecificationRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'type' => 'string',
            'groupId' => 'integer',
            'rowId' => 'integer',
            'groupTitle' => 'string',
            'nameTitle' => 'string',
            'valueTitle' => 'string',
            'charGroupNameId' => 'integer',
            'nameId' => 'integer',
            'valueId' => 'integer',
            'charNameId' => 'integer',
        ];
    }
}