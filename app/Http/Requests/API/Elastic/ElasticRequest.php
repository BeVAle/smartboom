<?php
/**
 * Created by PhpStorm.
 * User: BeVAle
 * Date: 20.03.2019
 * Time: 12:57
 */

namespace App\Http\Requests\API\Elastic;


use Illuminate\Foundation\Http\FormRequest;

class ElasticRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'query' => 'string',
        ];
    }
}