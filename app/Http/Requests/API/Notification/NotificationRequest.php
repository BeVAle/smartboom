<?php
/**
 * Created by PhpStorm.
 * User: BeVAle
 * Date: 08.03.2019
 * Time: 13:52
 */

namespace App\Http\Requests\API\Notification;


use Illuminate\Foundation\Http\FormRequest;

class NotificationRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    public function wantsJson()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => ['required','string'],
            'phone' => ['required','string'],
            'problem' => ['nullable','string'],
            'orderNumber' => ['nullable','string'],
        ];
    }
}