<?php
/**
 * Created by PhpStorm.
 * User: BeVAle
 * Date: 17.03.2019
 * Time: 22:38
 */

namespace App\Http\Requests\API\Notification;


use Illuminate\Foundation\Http\FormRequest;

class OrderRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    public function wantsJson()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|string',
            'phone' => 'required|string',
            'products' => 'required|array',
            'email' => 'sometimes|nullable|string',
            'city' => 'sometimes|nullable|string',
            'street' => 'sometimes|nullable|string',
            'houseNumber' => 'sometimes|nullable|string',
            'typeDelivery' => 'sometimes|nullable|integer',
            'typePayment' => 'sometimes|nullable|integer',
            'promoCode' => 'sometimes|nullable|string',
            'cost' => 'sometimes|nullable|numeric',
            'comments' => 'sometimes|nullable|string',
            'apartment' => 'sometimes|nullable|integer',
        ];
    }
}