<?php

namespace App\Entity\Characteristic\Models;

use Illuminate\Database\Eloquent\Model;

class CharacteristicGroupName extends Model
{
    protected $table = 'characteristic_group_names';

    protected $fillable = [
        'title',
    ];

    public $timestamps = false;

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function characteristicValues()
    {
        return $this->hasMany(CharacteristicName::class, 'characteristic_group_name_id', 'id');
    }

}
