<?php

namespace App\Entity\Characteristic\Models;

use App\Entity\Product\Models\ProductCharacteristic;
use Illuminate\Database\Eloquent\Model;

class CharacteristicValue extends Model
{
    protected $table = 'characteristic_values';

    protected $fillable = [
        'characteristic_name_id',
        'title',
    ];

    public $timestamps = false;

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function characteristicName()
    {
        return $this->hasOne(CharacteristicName::class, 'id', 'characteristic_name_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function productCharacteristics()
    {
        return $this->hasMany(ProductCharacteristic::class, 'characteristic_value_id', 'id');
    }
}
