<?php

namespace App\Entity\Characteristic\Models;

use Illuminate\Database\Eloquent\Model;

class CharacteristicName extends Model
{
    protected $table = 'characteristic_names';

    protected $fillable = [
        'characteristic_group_name_id',
        'title',
    ];

    public $timestamps = false;

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function characteristicGroupName()
    {
        return $this->hasOne(CharacteristicGroupName::class, 'id', 'characteristic_group_name_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function characteristicValues()
    {
        return $this->hasMany(CharacteristicValue::class, 'characteristic_name_id', 'id');
    }
}
