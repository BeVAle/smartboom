<?php
/**
 * Created by PhpStorm.
 * User: BeVAle
 * Date: 28.02.2019
 * Time: 15:26
 */

namespace App\Entity\Characteristic\Repositories;


use App\DTO\API\ProductSpecificationsDTO;
use App\Entity\Characteristic\Models\CharacteristicValue;
use App\Interfaces\RepositoryInterface;
use Illuminate\Database\Eloquent\Collection;

/**
 * @property CharacteristicValue $className
 */
class CharacteristicValueRepository implements RepositoryInterface
{
    private $className = CharacteristicValue::class;

    /**
     * @return CharacteristicValue[]|null
     */
    public function findAll()
    {
        return $this->className::all();
    }

    /**
     * @param int $id
     * @return CharacteristicValue
     */
    public function findById(int $id): CharacteristicValue
    {
        return $this->className::find($id);
    }

    /**
     * @param int $id
     * @throws \Exception
     */
    public function removeById(int $id): void
    {
        $this->findById($id)->delete();
    }

    /**
     * @param ProductSpecificationsDTO $dto
     * @return CharacteristicValue|null
     */
    public function save(ProductSpecificationsDTO $dto): ?CharacteristicValue
    {
        $characteristicValue = new CharacteristicValue([
            'characteristic_name_id' => $dto->charNameId,
            'title' => $dto->charValueTitle,
        ]);
        return $characteristicValue->save() ? $characteristicValue : null;
    }


    /**
     * @param int $charNameId
     * @return Collection
     */
    public function findListByCharName(int $charNameId): Collection
    {
        return $this->className::select('id', 'title')->where('characteristic_name_id', $charNameId)->get()->mapWithKeys(function ($item) {
            return [$item['id'] => $item['title']];
        });
    }
}