<?php
/**
 * Created by PhpStorm.
 * User: BeVAle
 * Date: 28.02.2019
 * Time: 15:26
 */

namespace App\Entity\Characteristic\Repositories;


use App\DTO\API\ProductSpecificationsDTO;
use App\Entity\Characteristic\Models\CharacteristicName;
use App\Interfaces\RepositoryInterface;
use Illuminate\Database\Eloquent\Collection;

/**
 * @property CharacteristicName $className
 */
class CharacteristicNameRepository implements RepositoryInterface
{
    private $className = CharacteristicName::class;

    /**
     * @return CharacteristicName[]|null
     */
    public function findAll()
    {
        return $this->className::all();
    }

    /**
     * @param int $id
     * @return CharacteristicName
     */
    public function findById(int $id): CharacteristicName
    {
        return $this->className::find($id);
    }

    /**
     * @param int $id
     * @throws \Exception
     */
    public function removeById(int $id): void
    {
        $this->findById($id)->delete();
    }

    /**
     * @param ProductSpecificationsDTO $dto
     * @return CharacteristicName|null
     */
    public function save(ProductSpecificationsDTO $dto): ?CharacteristicName
    {
        $characteristicName = new CharacteristicName([
            'characteristic_group_name_id' => $dto->charGroupNameId,
            'title' => $dto->charNameTitle,
        ]);
        return $characteristicName->save() ? $characteristicName : null;
    }


    /**
     * @param int $groupNameId
     * @return Collection
     */
    public function findListByCharGroupName(int $groupNameId): Collection
    {
        return $this->className::select('id', 'title')->where('characteristic_group_name_id', $groupNameId)->get()->mapWithKeys(function ($item) {
            return [$item['id'] => $item['title']];
        });
    }
}