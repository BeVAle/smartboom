<?php
/**
 * Created by PhpStorm.
 * User: BeVAle
 * Date: 28.02.2019
 * Time: 12:55
 */

namespace App\Entity\Characteristic\Repositories;


use App\DTO\API\ProductSpecificationsDTO;
use App\Entity\Characteristic\Models\CharacteristicGroupName;
use App\Interfaces\RepositoryInterface;
use Illuminate\Database\Eloquent\Collection;

/**
 * @property CharacteristicGroupName $className
 */
class CharacteristicGroupNameRepository implements RepositoryInterface
{
    private $className = CharacteristicGroupName::class;

    /**
     * @return CharacteristicGroupName[]|null
     */
    public function findAll()
    {
        return $this->className::all();
    }

    /**
     * @param int $id
     * @return CharacteristicGroupName
     */
    public function findById(int $id): CharacteristicGroupName
    {
        return $this->className::find($id);
    }

    /**
     * @param int $id
     * @throws \Exception
     */
    public function removeById(int $id): void
    {
        $this->findById($id)->delete();
    }

    /**
     * @param ProductSpecificationsDTO $dto
     * @return CharacteristicGroupName|null
     */
    public function save(ProductSpecificationsDTO $dto): ?CharacteristicGroupName
    {
        $characteristicGroupName = new CharacteristicGroupName(['title' => $dto->charGroupNameTitle]);
        return $characteristicGroupName->save() ? $characteristicGroupName : null;
    }

    /**
     * @return Collection
     */
    public function findList(): Collection
    {
        return $this->className::select('id', 'title')->get()->mapWithKeys(function ($item) {
            return [$item['id'] => $item['title']];
        });
    }


}