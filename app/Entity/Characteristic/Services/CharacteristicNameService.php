<?php
/**
 * Created by PhpStorm.
 * User: BeVAle
 * Date: 28.02.2019
 * Time: 15:27
 */

namespace App\Entity\Characteristic\Services;


use App\DTO\API\ProductSpecificationsDTO;
use App\Entity\Characteristic\Models\CharacteristicName;
use App\Entity\Characteristic\Repositories\CharacteristicNameRepository;

class CharacteristicNameService
{
    private $characteristicNameRepository;

    /**
     * CharacteristicNameService constructor.
     * @param CharacteristicNameRepository $characteristicNameRepository
     */
    public function __construct(CharacteristicNameRepository $characteristicNameRepository)
    {
        $this->characteristicNameRepository = $characteristicNameRepository;
    }

    /**
     * @param ProductSpecificationsDTO $dto
     * @return CharacteristicName|null
     */
    public function save(ProductSpecificationsDTO $dto): ?CharacteristicName
    {
        return $this->characteristicNameRepository->save($dto);
    }
}