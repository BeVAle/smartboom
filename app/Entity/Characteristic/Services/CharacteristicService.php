<?php
/**
 * Created by PhpStorm.
 * User: BeVAle
 * Date: 28.02.2019
 * Time: 16:41
 */

namespace App\Entity\Characteristic\Services;


use App\DTO\API\ProductSpecificationsDTO;

/**
 * @property CharacteristicGroupNameService $characteristicGroupNameService
 * @property CharacteristicNameService $characteristicNameService
 * @property CharacteristicValueService $characteristicValueService
 */
class CharacteristicService
{
    private $characteristicGroupNameService;
    private $characteristicNameService;
    private $characteristicValueService;

    /**
     * CharacteristicService constructor.
     * @param CharacteristicGroupNameService $characteristicGroupNameService
     * @param CharacteristicNameService $characteristicNameService
     * @param CharacteristicValueService $characteristicValueService
     */
    public function __construct(CharacteristicGroupNameService $characteristicGroupNameService,
                                CharacteristicNameService $characteristicNameService,
                                CharacteristicValueService $characteristicValueService)
    {
        $this->characteristicGroupNameService = $characteristicGroupNameService;
        $this->characteristicNameService = $characteristicNameService;
        $this->characteristicValueService = $characteristicValueService;
    }


    /**
     * @param ProductSpecificationsDTO $dto
     * @return \App\Entity\Characteristic\Models\CharacteristicGroupName|\App\Entity\Characteristic\Models\CharacteristicName|\App\Entity\Characteristic\Models\CharacteristicValue|null
     */
    public function saveCharacteristicByTypeAndReturn(ProductSpecificationsDTO $dto)
    {
        switch ($dto->type) {
            case 'groupName' :
                return $this->characteristicGroupNameService->save($dto);
            case 'name' :
                return $this->characteristicNameService->save($dto);
            case 'value' :
                return $this->characteristicValueService->save($dto);
            default :
                return null;
        }
    }
}