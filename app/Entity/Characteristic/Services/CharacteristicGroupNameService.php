<?php
/**
 * Created by PhpStorm.
 * User: BeVAle
 * Date: 28.02.2019
 * Time: 12:54
 */

namespace App\Entity\Characteristic\Services;


use App\DTO\API\ProductSpecificationsDTO;
use App\Entity\Characteristic\Models\CharacteristicGroupName;
use App\Entity\Characteristic\Repositories\CharacteristicGroupNameRepository;

class CharacteristicGroupNameService
{
    private $characteristicGroupNameRepository;

    /**
     * CharacteristicGroupNameService constructor.
     * @param CharacteristicGroupNameRepository $characteristicGroupNameRepository
     */
    public function __construct(CharacteristicGroupNameRepository $characteristicGroupNameRepository)
    {
        $this->characteristicGroupNameRepository = $characteristicGroupNameRepository;
    }

    /**
     * @param ProductSpecificationsDTO $dto
     * @return CharacteristicGroupName|null
     */
    public function save(ProductSpecificationsDTO $dto): ?CharacteristicGroupName
    {
        return $this->characteristicGroupNameRepository->save($dto);
    }


}