<?php
/**
 * Created by PhpStorm.
 * User: BeVAle
 * Date: 28.02.2019
 * Time: 15:28
 */

namespace App\Entity\Characteristic\Services;


use App\DTO\API\ProductSpecificationsDTO;
use App\Entity\Characteristic\Models\CharacteristicValue;
use App\Entity\Characteristic\Repositories\CharacteristicValueRepository;

class CharacteristicValueService
{
    private $characteristicValueRepository;

    /**
     * CharacteristicValueService constructor.
     * @param CharacteristicValueRepository $characteristicValueRepository
     */
    public function __construct(CharacteristicValueRepository $characteristicValueRepository)
    {
        $this->characteristicValueRepository = $characteristicValueRepository;
    }

    /**
     * @param ProductSpecificationsDTO $dto
     * @return CharacteristicValue|null
     */
    public function save(ProductSpecificationsDTO $dto): ?CharacteristicValue
    {
        return $this->characteristicValueRepository->save($dto);
    }
}