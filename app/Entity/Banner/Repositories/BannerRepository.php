<?php
/**
 * Created by PhpStorm.
 * User: BeVAle
 * Date: 21.03.2019
 * Time: 13:43
 */

namespace App\Entity\Banner\Repositories;


use App\entity\Banner\Models\Banner;
use App\DTO\Admin\Banner\BannerCreateDTO;
use App\Interfaces\RepositoryInterface;
use Illuminate\Database\Eloquent\Collection;

class BannerRepository implements RepositoryInterface
{
    private $className = Banner::class;

    /**
     * @return Banner[]|Collection|null
     */
    public function findAll()
    {
        return $this->className::all();
    }

    /**
     * @param array $ids
     * @return Banner[]
     */
    public function findAllById(array $ids)
    {
        return $this->className::query()->whereIn('id', $ids)->get()->all();
    }

    /**
     * @param int $id
     * @return Banner|null
     */
    public function findById(int $id): ?Banner
    {
        return $this->className::find($id);
    }

    /**
     * @param int $id
     * @throws \Exception
     */
    public function removeById(int $id): void
    {
        $this->findById($id)->delete();
    }

    /**
     * @param BannerCreateDTO $dto
     */
    public function save(BannerCreateDTO $dto): void
    {
        (new Banner([
            'title' => $dto->title,
            'logo' => $dto->logo,
            'cost' => $dto->cost,
            'link' => $dto->link,
            'brand_id' => $dto->brand_id,
            'product_type_id' => $dto->product_type_id,
            'desc' => $dto->desc,
        ]))->save();
    }

    /**
     * @param BannerCreateDTO $dto
     * @param int $id
     * @return Banner
     */
    public function update(BannerCreateDTO $dto, int $id): Banner
    {
        $banner = $this->findById($id);
        $banner->update($dto->toArray());
        $banner->save();
        return $banner;
    }

    /**
     * @return Collection
     */
    public function findList(): Collection
    {
        return $this->className::select('id', 'title')->get()->mapWithKeys(function ($item) {
            return [$item['id'] => $item['title']];
        });
    }

    /**
     * @return \Illuminate\Support\Collection
     */
    public function findAllWithCountProductToArray()
    {
        return collect($this->className::query()->get()->all())->map(function ($banner) {
            return [
                'id' => $banner->id,
                'title' => $banner->title,
                'cost' => $banner->cost,
                'link' => $banner->link,
                'brand_id' => $banner->brand_id,
                'brand_name' => ($type = $banner->brand) ? $type->title : '',
                'product_type_id' => $banner->product_type_id,
                'product_type_name' => ($type = $banner->productType) ? $type->title : '',
                'logo' => request()->getSchemeAndHttpHost() . $banner->logo,
                'desc' => $banner->desc,
            ];
        });
    }
}