<?php

namespace App\entity\Banner\Models;

use App\Entity\Brand\Models\Brand;
use App\Entity\ProductType\Models\ProductType;
use Illuminate\Database\Eloquent\Model;

class Banner extends Model
{
    protected $table = 'banners';

    protected $fillable = [
        'title',
        'logo',
        'cost',
        'link',
        'brand_id',
        'product_type_id',
        'desc',
    ];

    public $timestamps = false;

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function brand()
    {
        return $this->hasOne(Brand::class,'id','brand_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function productType()
    {
        return $this->hasOne(ProductType::class,'id','product_type_id');
    }

}
