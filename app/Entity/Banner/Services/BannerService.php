<?php
/**
 * Created by PhpStorm.
 * User: BeVAle
 * Date: 21.03.2019
 * Time: 13:45
 */

namespace App\Entity\Banner\Services;


use App\DTO\Admin\Banner\BannerCreateDTO;
use App\Entity\Banner\Repositories\BannerRepository;
use App\Traits\CropTrait;

class BannerService
{
    use CropTrait;

    private $bannerRepository;

    public function __construct(BannerRepository $bannerRepository)
    {
        $this->bannerRepository = $bannerRepository;
    }

    /**
     * @param BannerCreateDTO $dto
     */
    public function save(BannerCreateDTO $dto): void
    {
        $this->cropImage($dto->logo, [
            'cropX' => $dto->cropX,
            'cropY' => $dto->cropY,
            'cropWidth' => $dto->cropWidth,
            'cropHeight' => $dto->cropHeight,
        ]);
        $this->bannerRepository->save($dto);
    }

    public function update(BannerCreateDTO $dto, int $id): void
    {
        if (isset($dto->logo)){
            $this->cropImage($dto->logo, [
                'cropX' => $dto->cropX,
                'cropY' => $dto->cropY,
                'cropWidth' => $dto->cropWidth,
                'cropHeight' => $dto->cropHeight,
            ]);
        }
        $this->bannerRepository->update($dto,$id);
    }

    /**
     * @param int $id
     * @throws \Exception
     */
    public function removeById(int $id): void
    {
        $this->bannerRepository->removeById($id);
    }
}