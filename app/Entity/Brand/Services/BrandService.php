<?php
/**
 * Created by PhpStorm.
 * User: BeVAle
 * Date: 13.02.2019
 * Time: 17:52
 */

namespace App\Entity\Brand\Services;


use App\Entity\Brand\Repositories\BrandRepository;
use App\DTO\Admin\Brand\BrandCreateDTO;
use App\Traits\CropTrait;

class BrandService
{
    use CropTrait;

    private $brandRepository;

    public function __construct(BrandRepository $brandRepository)
    {
        $this->brandRepository = $brandRepository;
    }

    /**
     * @param BrandCreateDTO $dto
     */
    public function save(BrandCreateDTO $dto): void
    {
        $this->cropImage($dto->logo, [
            'cropX' => $dto->cropX,
            'cropY' => $dto->cropY,
            'cropWidth' => $dto->cropWidth,
            'cropHeight' => $dto->cropHeight,
        ]);
        $this->brandRepository->save($dto);
    }

    public function update(BrandCreateDTO $dto, int $id): void
    {
        if (isset($dto->logo)){
            $this->cropImage($dto->logo, [
                'cropX' => $dto->cropX,
                'cropY' => $dto->cropY,
                'cropWidth' => $dto->cropWidth,
                'cropHeight' => $dto->cropHeight,
            ]);
        }
        $this->brandRepository->update($dto,$id);
    }

    /**
     * @param int $id
     * @throws \Exception
     */
    public function removeById(int $id): void
    {
        $this->brandRepository->removeById($id);
    }

}