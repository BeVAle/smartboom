<?php
/**
 * Created by PhpStorm.
 * User: BeVAle
 * Date: 11.02.2019
 * Time: 16:12
 */

namespace App\Entity\Brand\Repositories;


use App\Entity\Brand\Models\Brand;
use App\DTO\Admin\Brand\BrandCreateDTO;
use App\Interfaces\RepositoryInterface;
use Illuminate\Database\Eloquent\Collection;

/**
 * @property Brand $className
 */
class BrandRepository implements RepositoryInterface
{
    private $className = Brand::class;

    /**
     * @return Brand[]|Collection|null
     */
    public function findAll()
    {
        return $this->className::all();
    }

    /**
     * @param array $ids
     * @return Brand[]
     */
    public function findAllById(array $ids)
    {
        return $this->className::query()->whereIn('id', $ids)->get()->all();
    }

    /**
     * @param int $id
     * @return Brand|null
     */
    public function findById(int $id): ?Brand
    {
        return $this->className::find($id);
    }

    /**
     * @param int $id
     * @throws \Exception
     */
    public function removeById(int $id): void
    {
        $this->findById($id)->delete();
    }

    /**
     * @param BrandCreateDTO $dto
     */
    public function save(BrandCreateDTO $dto): void
    {
        (new Brand([
            'title' => $dto->title,
            'logo' => $dto->logo,
        ]))->save();
    }

    /**
     * @param BrandCreateDTO $dto
     * @param int $id
     * @return Brand
     */
    public function update(BrandCreateDTO $dto, int $id): Brand
    {
        $brand = $this->findById($id);
        $brand->update($dto->toArray());
        $brand->save();
        return $brand;
    }

    /**
     * @return Collection
     */
    public function findList(): Collection
    {
        return $this->className::select('id', 'title')->get()->mapWithKeys(function ($item) {
            return [$item['id'] => $item['title']];
        });
    }

    /**
     * @return \Illuminate\Support\Collection
     */
    public function findAllWithCountProductToArray()
    {
        return collect($this->className::query()->get()->all())->map(function ($brand) {
            return [
                'id' => $brand->id,
                'title' => $brand->title,
                'logo' => request()->getSchemeAndHttpHost() . $brand->logo,
                'countProducts' => count($brand->products),
            ];
        });
    }
}