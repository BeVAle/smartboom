<?php

namespace App\Entity\Brand\Models;

use App\Entity\Product\Models\Product;
use Illuminate\Database\Eloquent\Model;

class Brand extends Model
{
    protected $table = 'brands';

    protected $fillable = [
        'title',
        'logo'
    ];

    protected $attributes = [
        'title' => 'Заголовок',
        'logo' => 'Логотип'
    ];

    public $timestamps = false;


    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function products()
    {
        return $this->hasMany(Product::class,'brand_id','id');
    }



}
