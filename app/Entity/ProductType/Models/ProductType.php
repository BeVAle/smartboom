<?php

namespace App\Entity\ProductType\Models;

use App\Entity\Product\Models\Product;
use Illuminate\Database\Eloquent\Model;

class ProductType extends Model
{
    protected $table = 'product_types';

    protected $fillable = [
        'title',
        'logo',
        'popular',
    ];

    protected $attributes = [
        'title' => 'Заголовок',
        'logo' => 'Логотип'
    ];

    public $timestamps = false;

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function products()
    {
        return $this->hasMany(Product::class, 'product_type_id', 'id');
    }

}