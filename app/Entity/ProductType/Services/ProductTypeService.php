<?php
/**
 * Created by PhpStorm.
 * User: BeVAle
 * Date: 14.02.2019
 * Time: 14:43
 */

namespace App\Entity\ProductType\Services;


use App\DTO\Admin\ProductType\ProductTypeDTO;
use App\Entity\ProductType\Repositories\ProductTypeRepository;
use App\Traits\CropTrait;

class ProductTypeService
{
    use CropTrait;

    private $productTypeRepository;

    /**
     * ProductTypeService constructor.
     * @param ProductTypeRepository $productTypeRepository
     */
    public function __construct(ProductTypeRepository $productTypeRepository)
    {
        $this->productTypeRepository = $productTypeRepository;
    }

    /**
     * @param ProductTypeDTO $dto
     */
    public function save(ProductTypeDTO $dto): void
    {
        $this->cropImage($dto->logo,[
            'cropX' => $dto->cropX,
            'cropY' => $dto->cropY,
            'cropWidth' => $dto->cropWidth,
            'cropHeight' => $dto->cropHeight,
        ]);
        $this->productTypeRepository->save($dto);
    }

    public function update(ProductTypeDTO $dto, int $id): void
    {
        if (isset($dto->logo)){
            $this->cropImage($dto->logo, [
                'cropX' => $dto->cropX,
                'cropY' => $dto->cropY,
                'cropWidth' => $dto->cropWidth,
                'cropHeight' => $dto->cropHeight,
            ]);
        }
        $this->productTypeRepository->update($dto,$id);
    }

    /**
     * @param int $id
     * @throws \Exception
     */
    public function removeById(int $id): void
    {
        $this->productTypeRepository->removeById($id);
    }

}