<?php
/**
 * Created by PhpStorm.
 * User: BeVAle
 * Date: 07.02.2019
 * Time: 15:59
 */

namespace App\Entity\ProductType\Repositories;

use App\DTO\Admin\ProductType\ProductTypeDTO;
use App\DTO\API\ProductTypeFilterDTO;
use App\Entity\Brand\Repositories\BrandRepository;
use App\Entity\Product\Models\Product;
use App\Entity\Product\Repositories\ProductRepository;
use App\Entity\ProductType\Models\ProductType;
use App\Factories\Admin\ProductType\ProductTypeFactory;
use App\Interfaces\RepositoryInterface;
use Illuminate\Database\Eloquent\Collection;

/**
 * @property ProductType $className
 * @property ProductTypeFactory $productTypeFactory
 */
class ProductTypeRepository implements RepositoryInterface
{
    private $className = ProductType::class;

    private $productTypeFactory;

    /**
     * ProductTypeRepository constructor.
     * @param ProductTypeFactory $productTypeFactory
     */
    public function __construct(ProductTypeFactory $productTypeFactory = null)
    {
        $this->productTypeFactory = $productTypeFactory;
    }

    /**
     * @return ProductType[]|null
     */
    public function findAll()
    {
        return $this->className::all();
    }

    /**
     * @param int $id
     * @return ProductType|null
     */
    public function findById(int $id): ?ProductType
    {
        return $this->className::find($id);
    }

    /**
     * @param int $id
     * @throws \Exception
     */
    public function removeById(int $id): void
    {
        $this->findById($id)->delete();
    }

    /**
     * @param ProductTypeDTO $dto
     */
    public function save(ProductTypeDTO $dto): void
    {
        $this->productTypeFactory->productType($dto)->save();
    }

    /**
     * @param ProductTypeDTO $dto
     * @param int $id
     * @return ProductType
     */
    public function update(ProductTypeDTO $dto, int $id): ProductType
    {
        $productType = $this->findById($id);
        $productType->update($dto->toArray());
        $productType->save();
        return $productType;
    }

    /**
     * @return Collection
     */
    public function findList(): Collection
    {
        return $this->className::query()->select('id', 'title')->get()->mapWithKeys(function ($item) {
            return [$item['id'] => $item['title']];
        });
    }


    /**
     * @param ProductTypeFilterDTO $dto
     * @return \Illuminate\Support\Collection
     */
    public function findAllWithCountProductToArray(ProductTypeFilterDTO $dto)
    {
        $query = $this->className::query();
        if (!is_null($dto->popular)) {
            $query->where('popular', 1);
        }
        $response = $query->get()->all();
        return collect($response)->map(function ($productType) {
            return ['product',
                'id' => $productType->id,
                'title' => $productType->title,
                'logo' => request()->getSchemeAndHttpHost() . $productType->logo,
                'popular' => $productType->popular,
                'countProducts' => (new ProductRepository())->getCountActiveByProductType($productType->id),
                'brands' => (new BrandRepository())->findAllById((new ProductRepository())->findBrandIdByProductTypeId($productType->id))
            ];
        });
    }
}