<?php

namespace App\Entity\Order\Models;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    protected $table = 'orders';

    protected $fillable = [
        'name',
        'phone',
        'email',
        'city',
        'street',
        'house_number',
        'type_delivery',
        'type_payment',
        'promo_code',
        'cost',
        'comments',
        'apartment',
    ];
}
