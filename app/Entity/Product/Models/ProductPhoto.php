<?php

namespace App\Entity\Product\Models;

use Illuminate\Database\Eloquent\Model;

class ProductPhoto extends Model
{
    protected $table = 'product_photos';

    protected $fillable = [
        'id',
        'product_id',
        'photo',
    ];

    public $timestamps = false;
}
