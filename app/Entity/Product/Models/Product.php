<?php

namespace App\Entity\Product\Models;

use App\Entity\Brand\Models\Brand;
use App\Entity\ProductType\Models\ProductType;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Product
 * @package App\Entity\Product\Models
 * @property ProductType $productType
 * @property Brand $brand
 * @property string $name
 * @property string $article
 */
class Product extends Model
{
    protected $table = 'products';

    protected $fillable = [
        'brand_id',
        'product_type_id',
        'name',
        'article',
        'old_cost',
        'cost',
        'color',
        'hit',
        'new',
        'description',
        'hide',
    ];

    public $timestamps = false;

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function brand()
    {
        return $this->hasOne(Brand::class,'id','brand_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function productType()
    {
        return $this->hasOne(ProductType::class,'id','product_type_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function photos()
    {
        return $this->hasMany(ProductPhoto::class,'product_id','id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function productCharacteristics()
    {
        return $this->hasMany(ProductCharacteristic::class,'product_id','id');
    }


}
