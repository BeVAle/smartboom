<?php

namespace App\Entity\Product\Models;

use App\Entity\Characteristic\Models\CharacteristicValue;
use Illuminate\Database\Eloquent\Model;

class ProductCharacteristic extends Model
{
    protected $table = 'product_characteristics';

    protected $fillable = [
        'product_id',
        'characteristic_value_id',
    ];

    public $timestamps = false;

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function product()
    {
        return $this->hasOne(Product::class, 'id', 'product_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function characteristicValue()
    {
        return $this->hasOne(CharacteristicValue::class, 'id', 'characteristic_value_id');
    }
}
