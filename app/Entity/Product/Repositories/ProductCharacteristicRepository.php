<?php
/**
 * Created by PhpStorm.
 * User: BeVAle
 * Date: 18.02.2019
 * Time: 14:32
 */

namespace App\Entity\Product\Repositories;


use App\Entity\Characteristic\Models\CharacteristicValue;
use App\Entity\Product\Models\ProductCharacteristic;
use App\Interfaces\RepositoryInterface;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Support\Facades\DB;

/**
 * @property ProductCharacteristic $className
 */
class ProductCharacteristicRepository implements RepositoryInterface
{
    private $className = ProductCharacteristic::class;

    /**
     * @return ProductCharacteristic[]|Collection|null
     */
    public function findAll()
    {
        return $this->className::all();
    }

    /**
     * @param int $id
     * @return ProductCharacteristic|null
     */
    public function findById(int $id): ?ProductCharacteristic
    {
        return $this->className::find($id);
    }

    /**
     * @param int $id
     * @throws \Exception
     */
    public function removeById(int $id): void
    {
        $this->findById($id)->delete();
    }


    /**
     * @param $id
     * @return \Illuminate\Support\Collection
     */
    public function getFullCharacteristicsById($id)
    {
        return $items = DB::table('product_characteristics')
            ->select([
                'characteristic_group_names.title AS characteristic_group_name',
                'characteristic_names.title AS characteristic_name',
                'characteristic_values.title AS characteristic_value'
            ])
            ->where('product_characteristics.product_id',$id)
            ->leftJoin('characteristic_values', 'product_characteristics.characteristic_value_id', '=', 'characteristic_values.id')
            ->leftJoin('characteristic_names', 'characteristic_values.characteristic_name_id', '=', 'characteristic_names.id')
            ->leftJoin('characteristic_group_names', 'characteristic_names.characteristic_group_name_id', '=', 'characteristic_group_names.id')
            ->orderBy('characteristic_group_name')
            ->get();
    }


    /**
     * Иногда появляетя баг, что в $item['value'] находится не айдишник характеристики а само значение (да, таки костыль)
     * @param int $id
     * @param array $values
     */
    public function saveByIdArr(int $id = 0, array $values = []): void
    {
        if ($id) {
            $this->findAllByProductIdQuery($id)->delete();
            if (!empty($values)) {
                foreach ($values as $value) {
                    foreach ($value as $item) {
                        if (isset($item['value'])) {
                            $charValueId = $item['value'];
                            if (!is_numeric($item['value']) && !CharacteristicValue::find($item['value'])) {
                                $charValue = new CharacteristicValue([
                                    'characteristic_name_id' =>$item['title'],
                                    'title' => $item['value']
                                ]);
                                $charValue->save();
                                $charValueId = $charValue->id;
                            }
                            (new ProductCharacteristic([
                                'product_id' => $id,
                                'characteristic_value_id' => $charValueId
                            ]))->save();
                        }
                    }
                }
            }
        }
    }

    /**
     * @param $id
     * @return \Illuminate\Database\Eloquent\Builder
     */
    private function findAllByProductIdQuery($id): Builder
    {
        return ProductCharacteristic::query()->where('product_id', $id);
    }

}