<?php
/**
 * Created by PhpStorm.
 * User: BeVAle
 * Date: 15.02.2019
 * Time: 13:37
 */

namespace App\Entity\Product\Repositories;


use App\Entity\Product\Models\ProductPhoto;
use App\Interfaces\RepositoryInterface;
use Illuminate\Database\Eloquent\Builder;

/**
 * @property ProductPhoto $className
 */
class ProductPhotoRepository implements RepositoryInterface
{
    private $className = ProductPhoto::class;

    /**
     * @param int $id
     * @return ProductPhoto
     */
    public function findById(int $id): ProductPhoto
    {
        return $this->className::find($id);
    }

    /**
     * @param int $id
     * @return array
     */
    public function findAllByProductId(int $id)
    {
        return $this->findAllByProductIdQuery($id)->get()->all();
    }

    /**
     * @return ProductPhoto[]
     */
    public function findAll()
    {
        return $this->className::all();
    }

    /**
     * @param int $id
     * @throws \Exception
     */
    public function removeById(int $id): void
    {
        $this->findById($id)->delete();
    }


    /**
     * @param int $id
     * @param array $photos
     */
    public function saveByIdArr(int $id = 0, array $photos = []): void
    {
        if ($id) {
            $this->findAllByProductIdQuery($id)->delete();
            if (!empty($photos)) {
                foreach ($photos as $photo) {
                    (new ProductPhoto([
                        'product_id' => $id,
                        'photo' => $photo
                    ]))->save();
                }
            }
        }

    }

    /**
     * @param $id
     * @return \Illuminate\Database\Eloquent\Builder
     */
    private function findAllByProductIdQuery($id): Builder
    {
        return ProductPhoto::query()->where('product_id', $id);
    }

}