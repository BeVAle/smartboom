<?php
/**
 * Created by PhpStorm.
 * User: BeVAle
 * Date: 07.02.2019
 * Time: 13:33
 */

namespace App\Entity\Product\Repositories;


use App\DTO\Admin\Product\ProductDTO;
use App\DTO\API\ProductFilterDTO;
use App\Entity\Product\Models\Product;
use App\Interfaces\RepositoryInterface;

/**
 * @property Product $className
 */
class ProductRepository implements RepositoryInterface
{
    private $className = Product::class;

    public function getQuery()
    {
        return Product::query();
    }

    /**
     * @return Product[]|null
     */
    public function findAll()
    {
        return $this->className::query()->where('hide',0)->get()->all();
    }

    /**
     * @param int $id
     * @return Product|\Illuminate\Database\Eloquent\Model|null
     */
    public function findById(int $id)
    {
        return $this->className::query()->where('id',$id)->with('productCharacteristics.characteristicValue.characteristicName.characteristicGroupName')->first();
    }

    /**
     * @param int $id
     * @throws \Exception
     */
    public function removeById(int $id): void
    {
        $product = $this->findById($id);
        $product->hide = 1;
        $product->save();
    }

    /**
     * @param ProductDTO $dto
     * @return Product
     */
    public function save(ProductDTO $dto): Product
    {
        $product = new Product([
            'brand_id' => $dto->brand_id,
            'product_type_id' => $dto->product_type_id,
            'name' => $dto->name,
            'article' => $dto->article,
            'old_cost' => $dto->old_cost ?? 0,
            'cost' => $dto->cost,
            'color' => $dto->color,
            'hit' => $dto->hit,
            'new' => $dto->new,
            'description' => $dto->description,
        ]);
        $product->save();
        return $product;
    }


    /**
     * @param ProductDTO $dto
     * @param int $id
     * @return Product
     */
    public function update(ProductDTO $dto, int $id): Product
    {
        $product = $this->findById($id);
        $product->update($dto->toArray());
        $product->save();
        return $product;
    }

    /**
     * @param ProductFilterDTO $form
     * @return array
     */
    public function filter(ProductFilterDTO $form)
    {
        $filter = $this->className::query()
            ->where('hide',0)
            ->with('productCharacteristics.characteristicValue.characteristicName.characteristicGroupName')
            ->whereBetween('cost', [$form->priceFrom, $form->priceTo]);
        if (!is_null($form->ids)) {
            $filter->findMany( $form->ids);
        }
        if (!is_null($form->category)) {
            $filter->where('product_type_id', $form->category);
        }
        if (!is_null($form->brands)) {
            $filter->whereIn('brand_id', $form->brands);
        }
        if (!is_null($form->hit)) {
            $filter->where('hit', 1);
        }
        if (!is_null($form->offset)) {
            $filter->skip((int)$form->offset);
        }
        if (!is_null($form->count)) {
            $filter->take((int)$form->count);
        }
        if (!is_null($form->sortPrice)) {
            if ($form->sortPrice == 1) {
                $filter->orderBy('cost', 'asc');
            } else {
                $filter->orderBy('cost', 'desc');
            }
        }
        if (!is_null($form->sortNew)) {
            if ($form->sortNew == 1) {
                $filter->orderBy('new', 'desc');
            }
        }
        if (!is_null($form->onlyNew)) {
            $filter->where('new', '=', 1);
        }
        $response = $filter->get()->all();
        return $response;
    }


    /**
     * @param int $productTypeId
     * @return array
     */
    public function findBrandIdByProductTypeId(int $productTypeId): array
    {
        $test = $this->className::query()
            ->select('brand_id')
            ->where('product_type_id', $productTypeId)
            ->groupBy('brand_id')
            ->get()
            ->pluck('brand_id')
            ->all();
        if (!empty($test)){
           return $test;
        }
        return [];

    }

    /**
     * @param $productTypeId
     * @return int
     */
    public function getCountActiveByProductType($productTypeId)
    {
        return $this->getQuery()->where('hide', 0)->where('product_type_id', $productTypeId)->get()->count();
    }
}