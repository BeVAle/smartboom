<?php
/**
 * Created by PhpStorm.
 * User: BeVAle
 * Date: 15.02.2019
 * Time: 13:32
 */

namespace App\Entity\Product\Services;


use App\DTO\Admin\Product\ProductDTO;
use App\Entity\Product\Models\Product;
use App\Entity\Product\Repositories\ProductCharacteristicRepository;
use App\Entity\Product\Repositories\ProductPhotoRepository;
use App\Entity\Product\Repositories\ProductRepository;

class ProductService
{
    private $productRepository;
    private $productPhotoRepository;
    private $productCharacteristicRepository;


    /**
     * ProductService constructor.
     * @param ProductRepository $productRepository
     * @param ProductPhotoRepository $productPhotoRepository
     * @param ProductCharacteristicRepository $productCharacteristicRepository
     */
    public function __construct(ProductRepository $productRepository, ProductPhotoRepository $productPhotoRepository, ProductCharacteristicRepository $productCharacteristicRepository)
    {
        $this->productRepository = $productRepository;
        $this->productPhotoRepository = $productPhotoRepository;
        $this->productCharacteristicRepository = $productCharacteristicRepository;
    }

    /**
     * @param ProductDTO $dto
     */
    public function save(ProductDTO $dto): void
    {
        /** @var Product $product */
        $product = $this->productRepository->save($dto);
        $this->productPhotoRepository->saveByIdArr($product->id, $dto->gallery);
        $this->productCharacteristicRepository->saveByIdArr($product->id, $dto->charGroup);
    }


    /**
     * @param ProductDTO $dto
     * @param int $id
     */
    public function update(ProductDTO $dto, int $id): void
    {
        $this->productRepository->update($dto, $id);
        $this->productPhotoRepository->saveByIdArr($id, $dto->gallery);
        $this->productCharacteristicRepository->saveByIdArr($id, $dto->charGroup);
    }

    /**
     * @param int $id
     * @throws \Exception
     */
    public function removeById(int $id): void
    {
        $this->productRepository->removeById($id);
    }
}