<?php
/**
 * Created by PhpStorm.
 * User: BeVAle
 * Date: 12.02.2019
 * Time: 15:41
 */

namespace App\Traits;


trait CropTrait
{
    /**
     * @param string $attribute
     * @param array $cropSettings [
     * @var int $cropX
     * @var int $cropY
     * @var int $cropWidth
     * @var int $cropHeight
     * ]
     * @return bool
     */
    public function cropImage(string $attribute, array $cropSettings): bool
    {
        if ($cropSettings['cropHeight'] == 0 || $cropSettings['cropWidth'] == 0) {
            return false;
        }

        $sourceImagePath = base_path('/public' . $attribute);
        $file_type = pathinfo($sourceImagePath, PATHINFO_EXTENSION);

        if ($file_type == 'jpg' || $file_type == 'jpeg') {
            $image = imagecreatefromjpeg($sourceImagePath);
        } elseif ($file_type == 'png') {
            $image = imagecreatefrompng($sourceImagePath);
        } else {
            return false;
        }

        $dest_r = imagecreatetruecolor($cropSettings['cropWidth'], $cropSettings['cropHeight']);
        /******* Add these 2 lines maintain transparency of PNG images ****/
        imagealphablending($dest_r, false);
        imagesavealpha($dest_r, true);
        $transparent = imagecolorallocatealpha($dest_r, 255, 255, 255, 127);
        imagefilledrectangle($dest_r, 0, 0, $cropSettings['cropWidth'], $cropSettings['cropHeight'], $transparent);
        /********end*********/
        $sizeImg = getimagesize($sourceImagePath);
        $widthImg = $sizeImg[0];
        $heightImg = $sizeImg[1];

        $def_width = $cropSettings['cropWidth'] / $cropSettings['cropWidth'] * $widthImg;
        $def_height = $cropSettings['cropHeight'] / $cropSettings['cropHeight'] * $heightImg;

        $dest_X = $cropSettings['cropWidth'] / $cropSettings['cropWidth'] * $cropSettings['cropX'];
        $dest_Y = $cropSettings['cropHeight'] / $cropSettings['cropHeight'] * $cropSettings['cropY'];

        if (!imagecopyresampled($dest_r, $image, -$dest_X, -$dest_Y, 0, 0, $def_width, $def_height, $widthImg, $heightImg)) {
            return false;
        }

        if ($file_type == 'jpg' || $file_type == 'jpeg') {
            imagejpeg($dest_r, $sourceImagePath, 81);
        } elseif ($file_type == 'png') {
            imagepng($dest_r, $sourceImagePath);
        }
        return true;
    }


    /**
     * @param string $attribute
     * @return bool
     */
    public function resizeImage(string $attribute): bool
    {
        $newWidth = 700;
        $sourceImagePath = base_path('/public' . $attribute);
        try {

            list($imageWidth, $imageHeight) = getimagesize($sourceImagePath);
        } catch (\Exception $e) {
            return false;
        }

        if (($imageWidth <= $newWidth)) {
            return true;
        }
        if (mime_content_type($sourceImagePath) == 'image/jpeg' || mime_content_type($sourceImagePath) == 'image/jpg') {
            $image = imagecreatefromjpeg($sourceImagePath);
        } elseif (mime_content_type($sourceImagePath) == 'image/png') {
            $image = imagecreatefrompng($sourceImagePath);
        } else {
            return false;
        }

        $sourceRatio = $imageWidth / $imageHeight;
        $newHeight = $newWidth / $sourceRatio;

        if ($imageWidth < $imageHeight) {
            //тогда высота новое изображение подходит по ширине, но не подходит по высоте (новое меньше), значит обрезаем по высоте
            $newTempHeight = $newHeight;
            $newTempWidth = $newHeight * $sourceRatio;
        } else {
            //тогда высота нового изображения подходит, но ширина нового, меньше, значит обрезаем по ширине
            $newTempWidth = $newWidth;
            $newTempHeight = (int)($newWidth / $sourceRatio);
        }

        $dest_r = imagecreatetruecolor($newTempWidth, $newTempHeight);
        /******* Add these 2 lines maintain transparency of PNG images ****/
        imagealphablending($dest_r, false);
        imagesavealpha($dest_r, true);
        $transparent = imagecolorallocatealpha($dest_r, 255, 255, 255, 127);
        imagefilledrectangle($dest_r, 0, 0, $newTempWidth, $newTempHeight, $transparent);
        /********end*********/

        if (!imagecopyresampled($dest_r, $image, 0, 0, 0, 0, $newTempWidth, $newTempHeight, $imageWidth, $imageHeight)) {
            return false;
        }
        // save only png or jpeg pictures
        if (mime_content_type($sourceImagePath) == 'image/jpeg') {
            imagejpeg($dest_r, $sourceImagePath, 100);
        } elseif (mime_content_type($sourceImagePath) == 'image/png') {
            imagepng($dest_r, $sourceImagePath);
        }
        return true;
    }
}