<?php
/**
 * Created by PhpStorm.
 * User: BeVAle
 * Date: 16.02.2019
 * Time: 12:56
 */

namespace App\Interfaces;


interface PresenterApiInterface
{
    public function toJson();
}