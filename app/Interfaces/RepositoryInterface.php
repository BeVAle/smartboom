<?php
/**
 * Created by PhpStorm.
 * User: BeVAle
 * Date: 12.02.2019
 * Time: 16:01
 */

namespace App\Interfaces;


interface RepositoryInterface
{
    /**
     * @return mixed
     */
    public function findAll();

    /**
     * @param int $id
     * @return mixed
     */
    public function findById(int $id);

    /**
     * @param int $id
     * @return mixed
     */
    public function removeById(int $id);
}