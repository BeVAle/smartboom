<?php
/**
 * Created by PhpStorm.
 * User: BeVAle
 * Date: 01.03.2019
 * Time: 13:32
 */

namespace App\Presenters\Admin\Product;


use App\Entity\Product\Repositories\ProductRepository;
use App\Presenters\General\ProductCharacteristicsPresenter;

class ProductViewPresenter
{
    public $product;
    public $productGallery = [];
    public $productCharacteristic = [];

    /**
     * ProductViewPresenter constructor.
     * @param int $id
     */
    public function __construct(int $id)
    {
        $this->product = (new ProductRepository())->findById($id);
        $this->productGallery = $this->product->photos;
        $this->productCharacteristic = (new ProductCharacteristicsPresenter())->getChars($this->product);
    }
}