<?php
/**
 * Created by PhpStorm.
 * User: BeVAle
 * Date: 11.02.2019
 * Time: 18:06
 */

namespace App\Presenters\Admin\Product;


use App\Entity\Brand\Repositories\BrandRepository;
use App\Entity\Characteristic\Repositories\CharacteristicGroupNameRepository;
use App\Entity\Product\Models\Product;
use App\Entity\Product\Models\ProductPhoto;
use App\Entity\Product\Repositories\ProductPhotoRepository;
use App\Entity\Product\Repositories\ProductRepository;
use App\Entity\ProductType\Repositories\ProductTypeRepository;
use App\Presenters\General\ProductCharacteristicsPresenter;
use Illuminate\Database\Eloquent\Collection;

/**
 * @property Collection $brands
 * @property Collection $productTypes
 * @property Product $product
 * @property ProductPhoto[] $productGallery
 * @property array $characteristics
 * @property Collection $characteristicGroupName
 */
class ProductManagePresenter
{
    public $brands;
    public $productTypes;
    public $product;
    public $productGallery = [];
    public $characteristicGroupName = [];
    public $characteristics = [];

    /**
     * ProductCreatePresenter constructor.
     * @param int $id
     */
    public function __construct(int $id = 0)
    {
        $this->brands = (new BrandRepository())->findList();
        $this->productTypes = (new ProductTypeRepository())->findList();
        $this->characteristicGroupName = (new CharacteristicGroupNameRepository())->findList();
        if ($id) {
            $this->product = (new ProductRepository())->findById($id);
            $this->productGallery = (new ProductPhotoRepository())->findAllByProductId($id);
            $this->characteristics = (new ProductCharacteristicsPresenter)->getCharsIds($this->product);
        } else {
            $this->product = new Product(['old_cost' => 0]);
        }

    }
}