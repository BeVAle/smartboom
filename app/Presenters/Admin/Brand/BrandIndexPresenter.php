<?php
/**
 * Created by PhpStorm.
 * User: BeVAle
 * Date: 12.02.2019
 * Time: 16:27
 */

namespace App\Presenters\Admin\Brand;


use App\Entity\Brand\Models\Brand;
use App\Entity\Brand\Repositories\BrandRepository;

/**
 * @property Brand[] $brands
 */
class BrandIndexPresenter
{
    public $brands;

    public function __construct()
    {
        $this->brands = (new BrandRepository())->findAll();
    }
}