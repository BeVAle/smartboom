<?php
/**
 * Created by PhpStorm.
 * User: BeVAle
 * Date: 14.02.2019
 * Time: 13:51
 */

namespace App\Presenters\Admin\Brand;


use App\Entity\Brand\Models\Brand;
use App\Entity\Brand\Repositories\BrandRepository;

/**
 * @property Brand $brand
 */
class BrandViewPresenter
{
    public $brand;

    /**
     * BrandViewPresenter constructor.
     * @param int $id
     */
    public function __construct(int $id)
    {
        $this->brand = (new BrandRepository())->findById($id);
    }
}