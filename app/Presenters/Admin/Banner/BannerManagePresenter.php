<?php
/**
 * Created by PhpStorm.
 * User: BeVAle
 * Date: 22.03.2019
 * Time: 17:41
 */

namespace App\Presenters\Admin\Banner;


use App\entity\Banner\Models\Banner;
use App\Entity\Banner\Repositories\BannerRepository;
use App\Entity\Brand\Repositories\BrandRepository;
use App\Entity\ProductType\Repositories\ProductTypeRepository;

class BannerManagePresenter
{
    public $brands;
    public $productTypes;
    public $banner;

    /**
     * BannerManagePresenter constructor.
     * @param int $id
     */
    public function __construct(int $id = 0)
    {
        $this->brands = (new BrandRepository())->findList();
        $this->productTypes = (new ProductTypeRepository())->findList();
        if ($id) {
            $this->banner = (new BannerRepository())->findById($id);
        } else {
            $this->banner = new Banner();
        }

    }
}