<?php
/**
 * Created by PhpStorm.
 * User: BeVAle
 * Date: 21.03.2019
 * Time: 13:53
 */

namespace App\Presenters\Admin\Banner;

use App\entity\Banner\Models\Banner;
use App\Entity\Banner\Repositories\BannerRepository;

/**
 * @property Banner $banner
 */
class BannerViewPresenter
{
    public $banner;

    /**
     * BannerViewPresenter constructor.
     * @param int $id
     */
    public function __construct(int $id)
    {
        $this->banner = (new BannerRepository())->findById($id);
    }
}