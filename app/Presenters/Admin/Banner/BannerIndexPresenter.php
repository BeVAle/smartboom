<?php
/**
 * Created by PhpStorm.
 * User: BeVAle
 * Date: 21.03.2019
 * Time: 13:52
 */

namespace App\Presenters\Admin\Banner;


use App\entity\Banner\Models\Banner;
use App\Entity\Banner\Repositories\BannerRepository;

/**
 * @property Banner[] $banners
 */
class BannerIndexPresenter
{
    public $banners;

    public function __construct()
    {
        $this->banners = (new BannerRepository())->findAll();
    }
}