<?php
/**
 * Created by PhpStorm.
 * User: BeVAle
 * Date: 20.03.2019
 * Time: 12:59
 */

namespace App\Presenters\Api\Elastic;


class ElasticPresenter
{
    private $elastic;

    public function __construct($elastic)
    {
        $this->elastic = $elastic;
    }

    public function toArray(){
        $data = [];
        if (isset($this->elastic['hits'])){
            if (!empty($this->elastic['hits']['hits'])){
                foreach ($this->elastic['hits']['hits'] as $item){
                    $data[] = $item['_source'];
                }
            }
        }
        return $data;

    }
}