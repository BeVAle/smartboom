<?php
/**
 * Created by PhpStorm.
 * User: BeVAle
 * Date: 17.03.2019
 * Time: 22:00
 */

namespace App\Presenters\Api\Notifications;


use App\Entity\Product\Repositories\ProductRepository;

class Order
{

    /**
     * @param array $productParameters
     * @return array
     */
    public function toArray(array $productParameters): array
    {
        $data = [];
        foreach ($productParameters as $itemParameters) {
            $product = (new ProductRepository())->findById($itemParameters['id']);
            $data[] = [
                'brand' => $product->brand->title,
                'productType' => $product->productType->title,
                'name' => $product->name,
                'article' => $product->article,
                'color' => $product->color,
                'cost' => $product->cost,
                'count' => $itemParameters['count'],
            ];
        }
        return $data;
    }
}