<?php
/**
 * Created by PhpStorm.
 * User: BeVAle
 * Date: 19.02.2019
 * Time: 17:17
 */

namespace App\Presenters\Api\Product;


use App\Entity\Product\Models\Product;
use App\Entity\Product\Models\ProductCharacteristic;

class ProductPresenter
{
    /**
     * @param Product $product
     * @return array
     */
    public function toArray(Product $product): array
    {
        return [
            'id' => $product->id,
            'ProductImgSrc' => (!empty($product->photos[0])) ? request()->getSchemeAndHttpHost() .$product->photos[0]->photo : '',
            'type' => ($type = $product->productType) ? $type->title : '',
            'name' => $product->name,
            'vendor_code' => $product->article,
            'hit' => (bool)$product->hit,
            'new' => (bool)$product->new,
            'oldPrice' => $product->old_cost,
            'price' => $product->cost,
            'color' => $product->color,
            'description' => $product->description,
            'photos' => $this->getPhotos($product),
            'characteristics' => $this->getChars($product),
        ];
    }

    /**
     * @param Product $product
     * @return array
     */
    public function getPhotos(Product $product): array
    {
        $arr = [];
        foreach ($product->photos as $photo) {
            $arr[] = request()->getSchemeAndHttpHost() . $photo->photo;
        }
        return $arr;
    }

    /**
     * @param Product $product
     * @return array
     */
    public function getChars(Product $product): array
    {
        $arr = [];
        foreach ($product->productCharacteristics as $key => $char) {
            if ($this->newGroupChar($arr, $char)) {
                $this->newCharacteristic($arr, $char);
            }
        }
        return $arr;
    }

    /**
     * @param array $arr
     * @param ProductCharacteristic $char
     * @return bool
     */
    protected function newGroupChar(&$arr, $char): bool
    {
        if (!empty($arr)) {
            foreach ($arr as $key => $item) {
                if ($item['title'] == $char->characteristicValue->characteristicName->characteristicGroupName->title) {
                    $arr[$key]['row'][] = [
                        'name' => $char->characteristicValue->characteristicName->title,
                        'val' => $char->characteristicValue->title,
                    ];
                    return false;
                }
            }
        }
        return true;
    }

    /**
     * @param array $arr
     * @param ProductCharacteristic $char
     */
    protected function newCharacteristic(&$arr, $char): void
    {
        $arr[] = [
            'title' => $char->characteristicValue->characteristicName->characteristicGroupName->title,
            'row' => [
                [
                    'name' => $char->characteristicValue->characteristicName->title,
                    'val' => $char->characteristicValue->title,
                ],
            ],
        ];
    }
}