<?php
/**
 * Created by PhpStorm.
 * User: BeVAle
 * Date: 21.03.2019
 * Time: 13:59
 */

namespace App\Presenters\Api\Banner;


use App\Entity\Banner\Repositories\BannerRepository;
use App\Interfaces\PresenterApiInterface;

class BannerPresenter implements PresenterApiInterface
{
    public $banners;
    public $bannersCount;

    /**
     * BrandPresenter constructor.
     */
    public function __construct()
    {
        $this->bannersCount = count($this->banners = (new BannerRepository())->findAllWithCountProductToArray());
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function toJson()
    {
        return response()->json([
            'success' => true,
            'brandsCount' => $this->bannersCount,
            'brands' => $this->banners,
        ]);
    }

}