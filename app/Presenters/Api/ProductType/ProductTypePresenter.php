<?php
/**
 * Created by PhpStorm.
 * User: BeVAle
 * Date: 15.02.2019
 * Time: 19:49
 */

namespace App\Presenters\Api\ProductType;


use App\DTO\API\ProductTypeFilterDTO;
use App\Entity\ProductType\Repositories\ProductTypeRepository;

class ProductTypePresenter
{
    public $productTypes;
    public $countProductTypes;
    
    /**
     * ProductTypePresenter constructor.
     * @param ProductTypeFilterDTO $dto
     */
    public function __construct(ProductTypeFilterDTO $dto)
    {
        $this->countProductTypes = count($this->productTypes = (new ProductTypeRepository())->findAllWithCountProductToArray($dto));
    }

    public function toJson()
    {
        return response()->json([
            'success' => true,
            'countProductTypes' => $this->countProductTypes,
            'productTypes' => $this->productTypes,
        ]);
    }

}