<?php
/**
 * Created by PhpStorm.
 * User: BeVAle
 * Date: 15.02.2019
 * Time: 19:28
 */

namespace App\Presenters\Api\Brand;


use App\Entity\Brand\Repositories\BrandRepository;
use App\Interfaces\PresenterApiInterface;

class BrandPresenter implements PresenterApiInterface
{
    public $brands;
    public $brandsCount;

    /**
     * BrandPresenter constructor.
     */
    public function __construct()
    {
        $this->brandsCount = count($this->brands = (new BrandRepository())->findAllWithCountProductToArray());
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function toJson()
    {
        return response()->json([
            'success' => true,
            'brandsCount' => $this->brandsCount,
            'brands' => $this->brands,
        ]);
    }

}