<?php
/**
 * Created by PhpStorm.
 * User: BeVAle
 * Date: 01.03.2019
 * Time: 13:25
 */

namespace App\Presenters\General;


use App\Entity\Characteristic\Repositories\CharacteristicNameRepository;
use App\Entity\Characteristic\Repositories\CharacteristicValueRepository;
use App\Entity\Product\Models\Product;

class ProductCharacteristicsPresenter
{
    private $characteristicNameRepository;
    private $characteristicValueRepository;

    /**
     * ProductCharacteristicsPresenter constructor.
     */
    public function __construct()
    {
        $this->characteristicNameRepository = new CharacteristicNameRepository();
        $this->characteristicValueRepository = new CharacteristicValueRepository();
    }

    /**
     * @param Product $product
     * @return array
     */
    public function getCharsIds(Product $product): array
    {
        return $this->getChars($product, 'id');
    }

    /**
     * @param Product $product
     * @param string $parameter
     * @return array
     */
    public function getChars(Product $product, $parameter = 'title'): array
    {
        $arr = [];
        foreach ($product->productCharacteristics as $key => $char) {
            if ($this->newGroupChar($arr, $char, $parameter)) {
                $this->newCharacteristic($arr, $char, $parameter);
            }
        }
        return $arr;
    }

    /**
     * @param $arr
     * @param $char
     * @param string $parameter
     * @return bool
     */
    protected function newGroupChar(&$arr, $char, $parameter = 'title'): bool
    {
        $charGroupName = $char->characteristicValue->characteristicName->characteristicGroupName[$parameter];
        $charName = $char->characteristicValue->characteristicName[$parameter];
        $charValue = $char->characteristicValue[$parameter];
        if (!empty($arr)) {
            foreach ($arr as $key => $item) {
                if ($item['title'] == $charGroupName) {
                    $arr[$key]['row'][] = [
                        'name' => $charName,
                        'val' => $charValue,
                        'characteristicName' => ($parameter == 'id') ? $this->characteristicNameRepository
                            ->findListByCharGroupName($charGroupName) : null,
                        'characteristicValue' => ($parameter == 'id') ? $this->characteristicValueRepository
                            ->findListByCharName($charName) : null,
                    ];
                    return false;
                }
            }
        }
        return true;
    }

    /**
     * @param $arr
     * @param $char
     * @param string $parameter
     */
    protected function newCharacteristic(&$arr, $char, $parameter = 'title'): void
    {
        $charGroupName = $char->characteristicValue->characteristicName->characteristicGroupName[$parameter];
        $charName = $char->characteristicValue->characteristicName[$parameter];
        $charValue = $char->characteristicValue[$parameter];
        $arr[] = [
            'title' => $charGroupName,
            'row' => [
                [
                    'name' => $charName,
                    'val' => $charValue,
                    'characteristicName' => ($parameter == 'id') ? $this->characteristicNameRepository
                        ->findListByCharGroupName($charGroupName) : null,
                    'characteristicValue' => ($parameter == 'id') ? $this->characteristicValueRepository
                        ->findListByCharName($charName) : null,
                ],
            ],
        ];
    }
}