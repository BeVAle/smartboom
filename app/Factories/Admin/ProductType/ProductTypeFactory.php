<?php
/**
 * Created by PhpStorm.
 * User: BeVAle
 * Date: 14.02.2019
 * Time: 14:56
 */

namespace App\Factories\Admin\ProductType;


use App\DTO\Admin\ProductType\ProductTypeDTO;
use App\Entity\ProductType\Models\ProductType;

class ProductTypeFactory
{
    /**
     * @param ProductTypeDTO $dto
     * @return ProductType
     */
    public function productType(ProductTypeDTO $dto): ProductType
    {
        return new ProductType([
            'title'=> $dto->title,
            'logo' => $dto->logo,
        ]);
    }
}