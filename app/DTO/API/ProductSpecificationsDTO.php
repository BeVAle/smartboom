<?php
/**
 * Created by PhpStorm.
 * User: BeVAle
 * Date: 28.02.2019
 * Time: 12:38
 */

namespace App\DTO\API;


use App\Http\Requests\API\Product\ProductSpecificationRequest;
/**
 * @param array $request
 * @param string $type
 * @param int $charGroupNameId
 * @param int $charNameId
 * @param int $charValueId
 * @param string $charGroupNameTitle
 * @param string $charNameTitle
 * @param string $charValueTitle
*/
class ProductSpecificationsDTO
{
    public $request;
    public $type;
    public $charGroupNameId;
    public $charNameId;
    public $charValueId;
    public $charGroupNameTitle;
    public $charNameTitle;
    public $charValueTitle;

    /**
     * ProductSpecificationsDTO constructor.
     * @param ProductSpecificationRequest $request
     */
    public function __construct(ProductSpecificationRequest $request)
    {
        $this->request = $request->all();
        $this->type = $this->request['type'] ?? null;
        $this->charGroupNameId = $this->request['groupId'] ?? null;
        $this->charNameId = $this->request['nameId'] ?? null;
        $this->charValueId = $this->request['valueId'] ?? null;
        $this->charGroupNameTitle = $this->request['groupTitle'] ?? null;
        $this->charNameTitle = $this->request['nameTitle'] ?? null;
        $this->charValueTitle = $this->request['valueTitle'] ?? null;
    }

}