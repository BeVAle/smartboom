<?php
/**
 * Created by PhpStorm.
 * User: BeVAle
 * Date: 12.02.2019
 * Time: 21:52
 */

namespace App\DTO\API;


use Illuminate\Http\Request;

class ProductFilterDTO
{
    public $ids;
    public $request;
    public $category;
    public $priceFrom = 0;
    public $priceTo = 9999999;
    public $brands;
    public $count;
    public $offset;
    public $sortPrice;
    public $sortNew;
    public $hit;
    public $onlyNew;

    /**
     * ProductFilterDTO constructor.
     * @param Request $request
     */
    public function __construct(Request $request)
    {
        $this->request = $request->all();
        $this->ids = !empty(($this->request['ids'])) ? $this->request['ids'] : null;
        $this->category = (isset($this->request['category'])) ? (int)$this->request['category'] : null;
        $this->brands =  !empty(($this->request['brands'])) ? $this->request['brands'] : null;
        $this->count =  $this->request['count'] ?? null;
        $this->offset = $this->request['offset'] ?? null;
        $this->hit = $this->request['hit'] ?? null;
        $this->onlyNew = $this->request['only_new'] ?? null;
        if (isset($this->request['price'])) {
            $this->priceFrom = (isset($this->request['price']['from'])) ? (int)$this->request['price']['from'] : 0;
            $this->priceTo = (isset($this->request['price']['to'])) ? (int)$this->request['price']['to'] : 9999999;
        }
        if (isset($this->request['sort'])) {
            $this->sortPrice = (isset($this->request['sort']['price'])) ? (int)$this->request['sort']['price'] : null;
            $this->sortNew = (isset($this->request['sort']['new'])) ? (int)$this->request['sort']['new'] : null;
        }
    }

}