<?php
/**
 * Created by PhpStorm.
 * User: BeVAle
 * Date: 19.02.2019
 * Time: 11:58
 */

namespace App\DTO\API;

use Illuminate\Http\Request;

class ProductTypeFilterDTO
{
    public $request;
    public $popular;

    /**
     * ProductTypeFilterDTO constructor.
     * @param Request $request
     */
    public function __construct(Request $request)
    {
        $this->request = $request->all();
        $this->popular = (isset($this->request['popular']) && !empty(($this->request['popular']))) ? $this->request['popular'] : null;

    }
}