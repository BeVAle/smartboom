<?php
/**
 * Created by PhpStorm.
 * User: BeVAle
 * Date: 13.02.2019
 * Time: 17:38
 */

namespace App\DTO\Admin\Brand;


class BrandCreateDTO
{
    public $file;
    public $cropX;
    public $cropY;
    public $cropHeight;
    public $cropWidth;
    public $title;
    public $logo;

    /**
     * BrandCreateDTO constructor.
     * @param array $options
     */
    public function __construct(array $options = [])
    {
        foreach ($options as $key => $value) {
            if (property_exists($this, $key)) {
                $this->$key = $value;
            }
        }
    }

    /**
     * @return array
     */
    public function toArray(): array
    {
        $arr = [];
        $class_vars = get_object_vars($this);
        foreach ($class_vars as $name => $value) {
            $arr[$name] = $value;
        }
        return $arr;
    }
}