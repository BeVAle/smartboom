<?php
/**
 * Created by PhpStorm.
 * User: BeVAle
 * Date: 14.02.2019
 * Time: 19:37
 */

namespace App\DTO\Admin\Product;


class ProductDTO
{
    public $id;
    public $brand_id;
    public $product_type_id;
    public $name;
    public $article;
    public $cost;
    public $old_cost;
    public $color;
    public $storage;
    public $hit;
    public $new;
    public $description;
    public $gallery = [];
    public $charGroup = [];

    /**
     * ProductDTO constructor.
     * @param array $options
     */
    public function __construct(array $options = [])
    {
        foreach ($options as $key => $value) {
            if (property_exists($this, $key)) {
                $this->$key = $value;
            }
        }
    }

    /**
     * @return array
     */
    public function toArray()
    {
        $arr = [];
        $class_vars = get_object_vars($this);
        foreach ($class_vars as $name => $value) {
            $arr[$name] = $value;
        }
        return $arr;
    }
}