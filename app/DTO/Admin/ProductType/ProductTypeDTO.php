<?php
/**
 * Created by PhpStorm.
 * User: BeVAle
 * Date: 14.02.2019
 * Time: 15:11
 */

namespace App\DTO\Admin\ProductType;


class ProductTypeDTO
{
    public $file;
    public $cropX;
    public $cropY;
    public $cropHeight;
    public $cropWidth;
    public $title;
    public $logo;
    public $popular;

    /**
     * ProductTypeDTO constructor.
     * @param array $options
     */
    public function __construct(array $options = [])
    {
        foreach ($options as $key => $value) {
            if (property_exists($this, $key)) {
                $this->$key = $value;
            }
        }
    }

    /**
     * @return array
     */
    public function toArray(): array
    {
        $arr = [];
        $class_vars = get_object_vars($this);
        foreach ($class_vars as $name => $value) {
            $arr[$name] = $value;
        }
        return $arr;
    }
}