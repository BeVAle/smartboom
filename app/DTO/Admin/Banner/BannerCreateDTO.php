<?php
/**
 * Created by PhpStorm.
 * User: BeVAle
 * Date: 21.03.2019
 * Time: 13:55
 */

namespace App\DTO\Admin\Banner;


class BannerCreateDTO
{
    public $file;
    public $cropX;
    public $cropY;
    public $cropHeight;
    public $cropWidth;
    public $title;
    public $logo;
    public $cost;
    public $link;
    public $brand_id;
    public $product_type_id;
    public $desc;

    /**
     * BrandCreateDTO constructor.
     * @param array $options
     */
    public function __construct(array $options = [])
    {
        foreach ($options as $key => $value) {
            if (property_exists($this, $key)) {
                $this->$key = $value;
            }
        }
    }

    /**
     * @return array
     */
    public function toArray(): array
    {
        $arr = [];
        $class_vars = get_object_vars($this);
        foreach ($class_vars as $name => $value) {
            $arr[$name] = $value;
        }
        return $arr;
    }
}