<?php
/**
 * Created by PhpStorm.
 * User: BeVAle
 * Date: 11.02.2019
 * Time: 11:28
 */

namespace App\Services;

use Illuminate\Http\Request;


class UploadFileService
{
    /**
     * @param Request $request
     * @return string (file path)
     */
    public function uploadImage(Request $request)
    {
        if ($file = $request->file('photo')) {
            $fileService = new FileService();
            $name = $fileService->getRandomFileName(base_path('/public/images/upload/'));
            $extension = $fileService->getExtensionByNameFile($file->getClientOriginalName());
            $newName = $name . '.' . $extension;
            $file->move(base_path('/public/images/upload/'), $newName);
            return '/images/upload/' . $newName;
        }
        return '';
    }
}