<?php
/**
 * Created by PhpStorm.
 * User: BeVAle
 * Date: 17.03.2019
 * Time: 23:29
 */

namespace App\Services;


use App\Entity\Brand\Models\Brand;
use App\Entity\Product\Models\Product;
use App\Entity\ProductType\Models\ProductType;
use Elasticsearch\ClientBuilder;

class ElasticService
{

    const INDEX_NAME = 'smartboom';
    private $hosts = ['localhost:9200'];
    private $client;

    public function __construct()
    {
        $this->client = ClientBuilder::create()
            ->setHosts($this->hosts)
            ->build();
    }

    /**
     * @return array
     */
    public function createIndex()
    {

        $params = [
            'index' => self::INDEX_NAME,
            'body' => [
                'settings' => [
                    "index.requests.cache.enable" => false,
                    'analysis' => [
                        'tokenizer' => [
                            'ngram_tokenizer' => [
                                'type' => 'edge_ngram',
                                'min_gram' => 1,
                                'max_gram' => 20,
                                "token_chars" => [
                                    "letter",
                                    "digit",
                                    "punctuation",
                                    "symbol"
                                ]
                            ]
                        ],
                        'analyzer' => [
                            'autocomplete' => [
                                'type' => 'custom',
                                'tokenizer' => 'ngram_tokenizer',
                                'filter' => [
                                    'lowercase',
                                ]
                            ],
                            "search_term_analyzer" => [
                                "type" => "custom",
                                "tokenizer" => "whitespace",
                                "filter" => [
                                    "lowercase",
                                    "asciifolding"
                                ]
                            ],
                        ],
                    ]
                ],
                'mappings' => [
                    'product' => [
                        '_all' => [
                            'analyzer' => 'autocomplete',
                            'search_analyzer' => 'search_term_analyzer',
                            "term_vector" => "with_positions_offsets",
                        ],
                        'properties' => [
                            'title' => ['type' => 'string', 'analyzer' => 'autocomplete'], //english
                            "id" => [
                                "type" => "integer",
                                "index" => "not_analyzed",
                                "include_in_all" => false
                            ],
                            "type" => [
                                "type" => "string",
                                "index" => "not_analyzed",
                                "include_in_all" => false
                            ],
                            "priority" => [
                                "type" => "integer",
                                "index" => "not_analyzed",
                                "include_in_all" => false
                            ],
                        ]
                    ],
                    'brand' => [
                        '_all' => [
                            'analyzer' => 'autocomplete',
                            'search_analyzer' => 'search_term_analyzer',
                            "term_vector" => "with_positions_offsets",
                        ],
                        'properties' => [
                            'title' => ['type' => 'string', 'analyzer' => 'autocomplete'], //english
                            "id" => [
                                "type" => "integer",
                                "index" => "not_analyzed",
                                "include_in_all" => false
                            ],
                            "type" => [
                                "type" => "string",
                                "index" => "not_analyzed",
                                "include_in_all" => false
                            ],
                            "priority" => [
                                "type" => "integer",
                                "index" => "not_analyzed",
                                "include_in_all" => false
                            ],
                        ]
                    ],
                    'productType' => [
                        '_all' => [
                            'analyzer' => 'autocomplete',
                            'search_analyzer' => 'search_term_analyzer',
                            "term_vector" => "with_positions_offsets",
                        ],
                        'properties' => [
                            'title' => ['type' => 'string', 'analyzer' => 'autocomplete'], //english
                            "id" => [
                                "type" => "integer",
                                "index" => "not_analyzed",
                                "include_in_all" => false
                            ],
                            "type" => [
                                "type" => "string",
                                "index" => "not_analyzed",
                                "include_in_all" => false
                            ],
                            "priority" => [
                                "type" => "integer",
                                "index" => "not_analyzed",
                                "include_in_all" => false
                            ],
                        ]
                    ],
                ],
            ],
        ];

        if ($this->client->indices()->exists(['index' => self::INDEX_NAME])) {
            $this->client->indices()->delete(['index' => self::INDEX_NAME]);
        }

        return $this->client->indices()->create($params);
    }

    /**
     * @param $query
     * @param $size
     * @return array
     */
    public function get($query, $size = 30)
    {
        $query = utf8_encode($query);
        $query = [
            'match' => [
                '_all' => [
                    'query' => $query,
                    "operator" => "and",
                ],
            ]
        ];
        $params = [
            'index' => self::INDEX_NAME,
            'size' => $size,
            '_source' => true,
            'body' => [
                'query' => $query,
                'highlight' => [
                    'fields' => [
                        "name*" => [
                            'require_field_match' => false,
                            'number_of_fragments' => 1,
                            "highlight_query" => $query
                        ]
                    ]
                ],
                'sort' => array(
                    array('priority' => array('order' => 'asc'))
                )
            ]
        ];
        return $this->client->search($params);
    }


    /**
     * @return array
     */
    private function getIndexConfig()
    {
        return [
            '_index' => self::INDEX_NAME,
            '_type' => self::INDEX_NAME,
        ];
    }

    /**
     * @param array $data
     * @return array
     */
    private function bulk($data)
    {
        $params = [
            'body' => $data,
        ];
        return $this->client->bulk($params);
    }

    /**
     * @param Brand[] $brands
     * @return array
     */
    public function bulkInsertBrands($brands)
    {
        $data = [];
        foreach ($brands as $key => $brand) {
            $data[] = [
                'index' => $this->getIndexConfig(),
            ];
            $data[] = [
                'id' => $brand->id,
                'title' => $brand->title,
                'type' => 'Brand',
                'priority' => $key,
            ];
        }
        return $this->bulk($data);
    }

    /**
     * @param ProductType[] $productTypes
     * @return array
     */
    public function bulkInsertProductTypes($productTypes)
    {
        $data = [];
        foreach ($productTypes as $key => $productType) {
            $data[] = [
                'index' => $this->getIndexConfig(),
            ];
            $data[] = [
                'id' => $productType->id,
                'title' => $productType->title,
                'type' => 'Product Type',
                'priority' => $key,
            ];
        }
        return $this->bulk($data);
    }

    /**
     * @param Product[] $products
     * @return array
     */
    public function bulkInsertProducts($products)
    {
        $data = [];
        foreach ($products as $key => $product) {
            $data[] = [
                'index' => $this->getIndexConfig(),
            ];
            $data[] = [
                'id' => $product->id,
                'title' => $product->name,
                'type' => 'Product',
                'priority' => $key,
            ];
        }
        return $this->bulk($data);
    }

}