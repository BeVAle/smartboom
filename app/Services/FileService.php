<?php
/**
 * Created by PhpStorm.
 * User: BeVAle
 * Date: 04.03.2019
 * Time: 16:53
 */

namespace App\Services;


class FileService
{
    public function getRandomFileName($path, $extension = '', $fullPath = false)
    {
        $extension = $extension ? '.' . $extension : '';
        $path = $path ? $path . '/' : '';

        do {
            $name = md5(microtime() . rand(0, 9999));
            $file = $path . $name . $extension;
        } while (file_exists($file));

        return ($fullPath === false) ? $name : $file;
    }

    public function getRandomFileNameWithPath($path, $extension = '', $host = '')
    {
        $extension = $extension ? '.' . $extension : '';
        $path = $path ? $path . '/' : '';

        do {
            $name = md5(microtime() . rand(0, 9999));
            $file = $path . $name . $extension;
        } while (file_exists($host . $file));

        return $file;
    }

    /**
     * @param string $name
     * @return mixed
     */
    public function getExtensionByNameFile(string $name)
    {
        $strArr = explode('.', $name);
        return $strArr[count($strArr) - 1];
    }

}