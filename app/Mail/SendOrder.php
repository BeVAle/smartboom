<?php
/**
 * Created by PhpStorm.
 * User: BeVAle
 * Date: 10.03.2019
 * Time: 22:15
 */

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class SendOrder extends Mailable
{
    use Queueable, SerializesModels;

    public $data;

    /**
     * SendOrder constructor.
     * @param $data
     */
    public function __construct($data)
    {
        $this->data = $data;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this
            ->subject('Новый Заказ')
            ->view('mail.email_send_order')
            ->with('data', $this->data);
    }
}