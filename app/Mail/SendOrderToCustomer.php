<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 09.04.2019
 * Time: 18:43
 */

namespace App\Mail;


use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class SendOrderToCustomer extends Mailable
{
    use Queueable, SerializesModels;

    public $data;

    /**
     * SendOrderToCustomer constructor.
     * @param $data
     */
    public function __construct($data)
    {
        $this->data = $data;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this
            ->subject('Ваш заказ успешно принят!')
            ->view('mail.email_send_order_to_customer')
            ->with('data', $this->data);
    }

}