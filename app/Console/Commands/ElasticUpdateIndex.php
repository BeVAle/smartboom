<?php

namespace App\Console\Commands;

use App\Entity\Brand\Repositories\BrandRepository;
use App\Entity\Product\Repositories\ProductRepository;
use App\Entity\ProductType\Repositories\ProductTypeRepository;
use App\Services\ElasticService;
use Illuminate\Console\Command;

class ElasticUpdateIndex extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'elastic:update-index';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'elastic update index';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        //        ini_set('memory_limit', -1);
        $startTime = microtime(true);
        $service = new ElasticService();
        $service->createIndex();
        $service->bulkInsertBrands((new BrandRepository())->findAll());
        $service->bulkInsertProductTypes((new ProductTypeRepository())->findAll());
        $service->bulkInsertProducts((new ProductRepository())->findAll());
        echo microtime(true) - $startTime;
    }
}
