<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Laravel</title>

        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet" type="text/css">
        <style>
            a.button {
                width: 300px;
                height: 50px;
                border-radius: 3px;
                border: 2px solid #000;
                display: flex;
                align-items: center;
                justify-content: center;
                margin: auto;
                
            }
        </style>
    </head>
    <body>
        <a href="/brand" class="button">Бренды</a>
    </body>
</html>
