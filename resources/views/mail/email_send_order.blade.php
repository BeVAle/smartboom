<p>Здравствуйте</p>
<p>Новый заказ</p>
<p>Номер заказа: {{$data['order_number']}}</p>
<p>Имя: {{$data['name']}}</p>
<p>Телефон: {{$data['phone']}}</p>
<p>E-mail: {{$data['email']}}</p>
<p>Город: {{$data['city']}}</p>
<p>Улица: {{$data['street']}}</p>
<p>Номер дома: {{$data['houseNumber']}}</p>
<p>Квартира: {{$data['apartment']}}</p>
<p>Способ доставки: {{$data['typeDelivery']}}</p>
<p>Способ оплаты: {{$data['typePayment']}}</p>
<p>Цена: {{$data['cost']}}</p>
<p>Промокод: {{$data['promoCode']}}</p>
<p>Комментарий: {{$data['comments']}}</p>

<p>Продукты:</p>
<br>
@foreach($data['products'] as $product)
    <div>Название: {{$product['brand']}} {{$product['name']}}</div>
    <div>Тип продукта: {{$product['productType']}}</div>
    <div>Артикул: {{$product['article']}} </div>
    <div>Цвет: {{$product['color']}} </div>
    <div>Цена товара: {{$product['cost']}} </div>
    <div>Количество: {{$product['count']}} </div>
    <br><br>
@endforeach
