<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 09.04.2019
 * Time: 18:45
 */
?>
<p>Здравствуйте, {{$data['name']}}</p>
<p>Ваш заказ {{$data['order_number']}} на сумму {{$data['cost']}} принят, и в ближайшее время с вами свяжется наш сотрудник для уточнения деталей</p>

<p>Заказанные вами продукты:</p>
<br>
@foreach($data['products'] as $product)
    <div>Название: {{$product['brand']}} {{$product['name']}}</div>
    <div>Тип продукта: {{$product['productType']}}</div>
    <div>Артикул: {{$product['article']}} </div>
    <div>Цвет: {{$product['color']}} </div>
    <div>Цена товара: {{$product['cost']}} </div>
    <div>Количество: {{$product['count']}} </div>
    <br><br>
@endforeach
<p>Наши контакты:</p>
<p>8 (901) 381-04-86</p>
<p>г. Москва ул. Краснодарская дом 66 офис 1</p>
<p>https://useit.store/</p>