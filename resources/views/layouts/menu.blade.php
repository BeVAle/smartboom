<li class="{{ Request::is('banner*') ? 'active' : '' }}">
    <a href="{!! route('banner.index') !!}"><i class="fa fa-edit"></i><span>Банеры</span></a>
</li>
<li class="{{ Request::is('brand*') ? 'active' : '' }}">
    <a href="{!! route('brand.index') !!}"><i class="fa fa-edit"></i><span>Бренды</span></a>
</li>
<li class="{{ Request::is('product-type*') ? 'active' : '' }}">
    <a href="{!! route('product-type.index') !!}"><i class="fa fa-edit"></i><span>Типы Продуктов</span></a>
</li>
<li class="{{ (Request::is('product/*')||Request::is('product')) ? 'active' : '' }}">
    <a href="{!! route('product.index') !!}"><i class="fa fa-edit"></i><span>Продукты</span></a>
</li>
