<div class="form-group col-sm-12">
    {!! Form::label('title', 'Имя') !!}
    {!! Form::text('title', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group col-sm-12">
    {!! Form::label('file', 'Логотип:') !!}
    {!! Form::file('file',['class'=>'upload-photo-input']) !!}
    <img src="" class="pre-review-photo">
    <div class="cropper-block">
        {!! Form::hidden('cropX',null,['id' => 'cropX'])  !!}
        {!! Form::hidden('cropY',null,['id' => 'cropY'])  !!}
        {!! Form::hidden('cropHeight',null,['id' => 'cropHeight'])  !!}
        {!! Form::hidden('cropWidth',null,['id' => 'cropWidth'])  !!}
        {!! Form::hidden('logo', null, ['class' => 'link-holder'])  !!}
    </div>
</div>
<div class="clearfix"></div>

@if(isset($presenter->brand->logo))
    <div class="form-group col-sm-12">
        <img src="{!! $presenter->brand->logo !!}" class="preview-photo-form">
    </div>
@endif

<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('brand.index') !!}" class="btn btn-default">Cancel</a>
</div>
