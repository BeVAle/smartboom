<div class="form-group">
    {!! Form::label('id', 'Id:') !!}
    <p>{!! $presenter->brand->id !!}</p>
</div>
<div class="form-group">
    {!! Form::label('title', 'Title:') !!}
    <p>{!! $presenter->brand->title !!}</p>
</div>
<div class="form-group">
    {!! Form::label('logo', 'Logo:') !!}
    <p>{{ Html::image($presenter->brand->logo , 'logo',['width' => 150, 'height' => 'auto' ]) }}</p>
</div>



