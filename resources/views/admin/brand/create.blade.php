@extends('layouts.app')
@section('content')
    <section class="content-header">
        <h1>Бренды</h1>
    </section>
    <div class="content">
        @include('adminlte-templates::common.errors')
        <div class="box box-primary">
            <div class="box-body">
                <div class="row">
                    {!! Form::open(['route' => 'brand.store','name'=>'form-uploader','method'=>'post','enctype'=>'multipart/form-data']) !!}
                    @include('admin.brand.fields')
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
@endsection