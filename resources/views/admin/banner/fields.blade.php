<div class="form-group col-sm-12">
    {!! Form::label('brand_id', 'Бренд') !!}
    {!! Form::select('brand_id', $presenter->brands,$presenter->banner->brand_id,['class' => 'form-control']); !!}
</div>

<div class="form-group col-sm-12">
    {!! Form::label('product_type_id', 'Тип продукта') !!}
    {!! Form::select('product_type_id', $presenter->productTypes,$presenter->banner->product_type_id,['class' => 'form-control']); !!}
</div>

<div class="form-group col-sm-12">
    {!! Form::label('title', 'Имя') !!}
    {!! Form::text('title', $presenter->banner->title, ['class' => 'form-control']) !!}
</div>

<div class="form-group col-sm-12">
    {!! Form::label('cost', 'Цена') !!}
    {!! Form::text('cost', $presenter->banner->cost, ['class' => 'form-control']) !!}
</div>

<div class="form-group col-sm-12">
    {!! Form::label('link', 'Ссылка') !!}
    {!! Form::text('link', $presenter->banner->link, ['class' => 'form-control']) !!}
</div>

<div class="form-group col-sm-12">
    {!! Form::label('desc', 'Описание') !!}
    {!! Form::text('desc', $presenter->banner->desc, ['class' => 'form-control']) !!}
</div>

<div class="form-group col-sm-12">
    {!! Form::label('file', 'Логотип:') !!}
    {!! Form::file('file',['class'=>'upload-photo-input']) !!}
    <img src="" class="pre-review-photo">
    <div class="cropper-block">
        {!! Form::hidden('cropX',0,['id' => 'cropX'])  !!}
        {!! Form::hidden('cropY',0,['id' => 'cropY'])  !!}
        {!! Form::hidden('cropHeight',0,['id' => 'cropHeight'])  !!}
        {!! Form::hidden('cropWidth',0,['id' => 'cropWidth'])  !!}
        {!! Form::hidden('logo', null, ['class' => 'link-holder'])  !!}
    </div>
</div>
<div class="clearfix"></div>

@if(isset($presenter->banner->logo))
    <div class="form-group col-sm-12">
        <img src="{!! $presenter->banner->logo !!}" class="preview-photo-form">
    </div>
@endif

<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('banner.index') !!}" class="btn btn-default">Cancel</a>
</div>
