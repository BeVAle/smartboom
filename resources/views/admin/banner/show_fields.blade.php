<div class="form-group">
    {!! Form::label('id', 'Id:') !!}
    <p>{!! $presenter->banner->id !!}</p>
</div>
<div class="form-group">
    {!! Form::label('brand_id', 'Бренд') !!}
    <p>{!!$presenter->banner->brand_id!!}</p>
</div>
<div class="form-group">
    {!! Form::label('product_type_id', 'Тип продукта') !!}
    <p>{!!$presenter->banner->product_type_id!!}</p>
</div>
<div class="form-group">
    {!! Form::label('title', 'Название:') !!}
    <p>{!! $presenter->banner->title !!}</p>
</div>
<div class="form-group">
    {!! Form::label('link', 'Ссылка:') !!}
    <p>{!! $presenter->banner->link !!}</p>
</div>
<div class="form-group">
    {!! Form::label('cost', 'Цена:') !!}
    <p>{!! $presenter->banner->cost !!}</p>
</div>
<div class="form-group">
    {!! Form::label('desc', 'Описание:') !!}
    <p>{!! $presenter->banner->desc !!}</p>
</div>
<div class="form-group">
    {!! Form::label('logo', 'Logo:') !!}
    <p>{{ Html::image($presenter->banner->logo , 'logo',['width' => 150, 'height' => 'auto' ]) }}</p>
</div>



