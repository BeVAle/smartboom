<table class="table table-responsive" id="products-table">
    <thead>
    <tr>
        <th>Бренд</th>
        <th>Тип</th>
        <th>Название</th>
        <th>Logo</th>
        <th colspan="3">Action</th>
    </tr>
    </thead>
    <tbody>
    @foreach($banners as $banner)
        <tr>
            <td>{!! $banner->brand_id !!}</td>
            <td>{!! $banner->product_type_id !!}</td>
            <td>{!! $banner->title !!}</td>
            <td>{{ Html::image($banner->logo , 'logo',['width' => 150, 'height' => 'auto' ]) }}</td>
            <td>
                {!! Form::open(['route' => ['banner.destroy', $banner->id], 'method' => 'delete']) !!}
                <div class='btn-group'>
                    <a href="{!! route('banner.show', [$banner->id]) !!}" class='btn btn-default btn-xs'><i
                                class="glyphicon glyphicon-eye-open"></i></a>
                    <a href="{!! route('banner.edit', [$banner->id]) !!}" class='btn btn-default btn-xs'><i
                                class="glyphicon glyphicon-edit"></i></a>
                    {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                </div>
                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>