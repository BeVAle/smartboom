<table class="table table-responsive" id="products-table">
    <thead>
    <tr>
        <th>Заголовок</th>
        <th>Популярная категория</th>
        <th>Логотип</th>
        <th colspan="3">Действия</th>
    </tr>
    </thead>
    <tbody>
    @foreach($productTypes as $productType)
        <tr>
            <td>{!! $productType->title !!}</td>
            <td>{!! $productType->popular !!}</td>
            <td>{{ Html::image($productType->logo , 'logo',['width' => 150, 'height' => 'auto' ]) }}</td>
            <td>
                {!! Form::open(['route' => ['product-type.destroy', $productType->id], 'method' => 'delete']) !!}
                <div class='btn-group'>
                    <a href="{!! route('product-type.show', [$productType->id]) !!}" class='btn btn-default btn-xs'><i
                                class="glyphicon glyphicon-eye-open"></i></a>
                    <a href="{!! route('product-type.edit', [$productType->id]) !!}" class='btn btn-default btn-xs'><i
                                class="glyphicon glyphicon-edit"></i></a>
                    {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                </div>
                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>