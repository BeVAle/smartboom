@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>Бренды</h1>
    </section>
    <div class="content">
        @include('adminlte-templates::common.errors')
        <div class="box box-primary">
            <div class="box-body">
                <div class="row">
                    {!! Form::model($productType, ['route' => ['product-type.update', $productType->id],'name'=>'form-uploader','method'=>'patch','enctype'=>'multipart/form-data']) !!}
                    @include('admin.product-type.fields')
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
@endsection