@extends('layouts.app')
@section('content')
    <section class="content-header">
        <h1>Тип продуктов</h1>
    </section>
    <div class="content">
        @include('adminlte-templates::common.errors')
        <div class="box box-primary">
            <div class="box-body">
                <div class="row">
                    {!! Form::open(['route' => 'product-type.store','name'=>'form-uploader','method'=>'post','enctype'=>'multipart/form-data']) !!}
                    @include('admin.product-type.fields')
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
@endsection