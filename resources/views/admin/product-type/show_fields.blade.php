<div class="form-group">
    {!! Form::label('id', 'Id:') !!}
    <p>{!! $productType->id !!}</p>
</div>
<div class="form-group">
    {!! Form::label('title', 'Title:') !!}
    <p>{!! $productType->title !!}</p>
</div>

<div class="form-group col-sm-12">
    {!! Form::label('popular', 'Популярная категория') !!}
    <p>{!!$productType->hit!!}</p>
</div>
<div class="form-group">
    {!! Form::label('logo', 'Logo:') !!}
    <p>{{ Html::image($productType->logo , 'logo',['width' => 150, 'height' => 'auto' ]) }}</p>
</div>



