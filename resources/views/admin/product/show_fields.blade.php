<div class="form-group col-sm-12">
    {!! Form::label('id', 'Id:') !!}
    <p>{!! $productPresenter->product->id !!}</p>
</div>
<div class="form-group col-sm-12">
    {!! Form::label('brand_id', 'Бренд') !!}
    <p>{!!$productPresenter->product->brand_id!!}</p>
</div>
<div class="form-group col-sm-12">
    {!! Form::label('product_type_id', 'Тип продукта') !!}
    <p>{!!$productPresenter->product->product_type_id!!}</p>
</div>
<div class="form-group col-sm-12">
    {!! Form::label('name', 'Название') !!}
    <p>{!!$productPresenter->product->name!!}</p>
</div>
<div class="form-group col-sm-12">
    {!! Form::label('article', 'Артикул') !!}
    <p>{!!$productPresenter->product->article!!}</p>
</div>
<div class="form-group col-sm-12">
    {!! Form::label('old_cost', 'Старая цена') !!}
    <p>{!!$productPresenter->product->old_cost!!}</p>
</div>
<div class="form-group col-sm-12">
    {!! Form::label('cost', 'Цена') !!}
    <p>{!!$productPresenter->product->cost!!}</p>
</div>
<div class="form-group col-sm-12">
    {!! Form::label('color', 'Цвет') !!}
    <p>{!!$productPresenter->product->color!!}</p>
</div>
<div class="form-group col-sm-12">
    {!! Form::label('hit', 'Хит') !!}
    <p>{!!$productPresenter->product->hit!!}</p>
</div>
<div class="form-group col-sm-12">
    {!! Form::label('new', 'Новинка') !!}
    <p>{!!$productPresenter->product->new!!}</p>
</div>
<div class="form-group col-sm-12">
    {!! Form::label('description', 'Описание') !!}
    <p>{!!$productPresenter->product->description!!}</p>
</div>

<section class="col-sm-12">
    <h2 class="">Галерея:</h2>
    <div class="gallery main">
        @if (!empty($productPresenter->productGallery))
            @foreach($productPresenter->productGallery as $item)
                <div class="js-gallery-item">
                    <div class="img-wrap">
                        <img class="js-img" src="{{$item->photo}}"/>
                    </div>
                </div>
            @endforeach
        @endif
    </div>
</section>

<section class="specifications col-sm-12">
    <h2>Характеристики</h2>
    <div class="groups">
        <div class="blocks-group">
            @foreach($productPresenter->productCharacteristic as $char)
                <div class="group">
                    <div class="group-name-wrap">
                        <div class="form-group">
                            <div><?= $char['title'] ?></div>
                        </div>
                    </div>
                    <div class="rows">
                        @foreach($char['row'] as $row)
                            <div class="row-wrap">
                                <div class="key">
                                    <div class="form-group">
                                        <div><?=$row['name'] ?> </div>
                                    </div>
                                </div>
                                <div class="val">
                                    <div class="form-group">
                                        <div><?=$row['val'] ?></div>
                                    </div>
                                </div>
                            </div>
                        @endforeach
                    </div>
                </div>
            @endforeach
        </div>
    </div>
</section>