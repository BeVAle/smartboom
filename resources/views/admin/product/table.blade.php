<table class="table table-responsive" id="products-table">
    <thead>
    <tr>
        <th>Бренд</th>
        <th>Тип</th>
        <th>Название</th>
        <th>Артикул</th>
        <th>Цена</th>
        <th>Цвет</th>
        <th colspan="3">Action</th>
    </tr>
    </thead>
    <tbody>
    @foreach($products as $product)
        <tr>
            <td>{!! $product->brand_id !!}</td>
            <td>{!! $product->product_type_id !!}</td>
            <td>{!! $product->name !!}</td>
            <td>{!! $product->article !!}</td>
            <td>{!! $product->cost !!}</td>
            <td>{!! $product->color !!}</td>
            <td>
                {!! Form::open(['route' => ['product.destroy', $product->id], 'method' => 'delete']) !!}
                <div class='btn-group'>
                    <a href="{!! route('product.show', [$product->id]) !!}" class='btn btn-default btn-xs'><i
                                class="glyphicon glyphicon-eye-open"></i></a>
                    <a href="{!! route('product.edit', [$product->id]) !!}" class='btn btn-default btn-xs'><i
                                class="glyphicon glyphicon-edit"></i></a>
                    {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                </div>
                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>