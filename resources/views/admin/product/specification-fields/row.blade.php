<div class="row-wrap" data-row-id="{!! $rowId !!}">
    <button title="remove row" class="remove remove-row">x</button>
    <div class="key">
        <div class="form-group">
            <label for="">Параметр</label>
            {!! Form::select('charGroup['.$groupId.']['.$rowId.'][title]',$characteristicName,null,['class'=>'group-'.$groupId.'-row-key-'.$rowId.' form-control', 'style'=>'width:100%']) !!}
        </div>
    </div>
    <div class="val" style="display: none">
        <div class="form-group">
            <label for="">Значение</label>
            {!! Form::select('charGroup['.$groupId.']['.$rowId.'][value]',$characteristicValue,null,['class'=>'group-'.$groupId.'-row-val-'.$rowId.' form-control', 'style'=>'width:100%']) !!}
        </div>
    </div>
</div>