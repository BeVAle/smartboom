<div class="group" data-group-id="{!!$groupId!!}">
    <button title="remove group" class="remove remove-group">x</button>
    <div class="group-name-wrap">
        <div class="form-group">
            <label for="">Группа параметров</label>
            {!! Form::select('charGroup['.$groupId.'][groupName]',$characteristicGroupName,null,['class'=>'group-select'.$groupId.' js-select form-control', 'style'=>'width:100%']) !!}
        </div>
    </div>
    <div class="rows"></div>
    <div class="row-buttons-wrap">
        <button class="create btn btn-primary js-create-new-row create-row">create new row</button>
    </div>
</div>


