<div class="form-group col-sm-12">
    {!! Form::label('brand_id', 'Бренд') !!}
    {!! Form::select('brand_id', $productPresenter->brands,$productPresenter->product->brand_id,['class' => 'form-control']); !!}
</div>

<div class="form-group col-sm-12">
    {!! Form::label('product_type_id', 'Тип продукта') !!}
    {!! Form::select('product_type_id', $productPresenter->productTypes,$productPresenter->product->product_type_id,['class' => 'form-control']); !!}
</div>

<div class="form-group col-sm-12">
    {!! Form::label('name', 'Название') !!}
    {!! Form::text('name', $productPresenter->product->name, ['class' => 'form-control']) !!}
</div>

<div class="form-group col-sm-12">
    {!! Form::label('article', 'Артикул') !!}
    {!! Form::text('article', $productPresenter->product->article, ['class' => 'form-control']) !!}
</div>

<div class="form-group col-sm-12">
    {!! Form::label('old_cost', 'Старая цена') !!}
    {!! Form::text('old_cost', $productPresenter->product->old_cost, ['class' => 'form-control']) !!}
</div>

<div class="form-group col-sm-12">
    {!! Form::label('cost', 'Цена') !!}
    {!! Form::text('cost', $productPresenter->product->cost, ['class' => 'form-control']) !!}
</div>

<div class="form-group col-sm-12">
    {!! Form::label('color', 'Цвет') !!}
    {!! Form::text('color', $productPresenter->product->color, ['class' => 'form-control']) !!}
</div>

<input type="hidden" name="hit" value="0">
<div class="form-group col-sm-12">
    {!! Form::label('hit', 'Хит') !!}
    {!! Form::checkbox('hit',  '1' ) !!}
</div>

<input type="hidden" name="new" value="0">
<div class="form-group col-sm-12">
    {!! Form::label('new', 'Новинка') !!}
    {!! Form::checkbox('new',  '1' ) !!}
</div>

<div class="form-group col-sm-12">
    {!! Form::label('description', 'Описание') !!}
    {!! Form::textarea('description', $productPresenter->product->description, ['class' => 'form-control kek', 'id'=>'ckeditor']) !!}
</div>
<div class="clearfix"></div>
<div class="col-sm-12">
    <h2 class="">Gallery:</h2>
    <label title="Upload image file" for="inputPostersImage" class="btn btn-primary button-gallery">
        <input type="file" accept="image/*" id="inputPostersImage" class="hide gallery-uploader" multiple="multiple">
        Add
    </label>
    <div class="gallery main">
        @if (!empty($productPresenter->productGallery))
        @foreach($productPresenter->productGallery as $item)
            <div class="js-gallery-item">
                <div class="img-wrap">
                    <img class="js-img" src="{{$item->photo}}"/>
                    <input type="text" class="hide filename" name="gallery[]"
                        value="{{$item->photo}}">
                </div>
                <div class="text-wrap">
                    <button>Удалить</button>
                </div>
            </div>
        @endforeach
        @endif
    </div>
</div>

<section class="specifications col-sm-12">
    <h2>Характеристики</h2>
    <div class="groups">
        <div class="blocks-group">
            @foreach($productPresenter->characteristics as $keyChar => $char)
                <?php $groupId = $keyChar+ 1;  ?>
            <div class="group" data-group-id="{!! $groupId !!}">
                <button title="remove group" class="remove remove-group">x</button>
                <div class="group-name-wrap">
                    <div class="form-group">
                        <label for="">Группа параметров</label>
                        {!! Form::select('charGroup['.$groupId.'][groupName]', $productPresenter->characteristicGroupName,$char['title'],['class'=>'group-select'.$groupId.' form-control', 'style'=>'width:100%']) !!}
                    </div>
                </div>
                <div class="rows">
                    @foreach($char['row'] as $keyRow =>$row)
                        <?php $rowId = $keyRow + 1; ?>
                        <div class="row-wrap" data-row-id="{!! $rowId !!}">
                            <button title="remove row" class="remove remove-row">x</button>
                            <div class="key">
                                <div class="form-group">
                                    <label for="">Параметр</label>
                                    {!! Form::select('charGroup['.$groupId.']['.$rowId.'][title]',$row['characteristicName'],$row['name'],['class'=>'group-'.$groupId.'-row-key-'.$rowId.' form-control', 'style'=>'width:100%']) !!}
                                </div>
                            </div>
                            <div class="val">
                                <div class="form-group">
                                    <label for="">Значение</label>
                                    {!! Form::select('charGroup['.$groupId.']['.$rowId.'][value]',$row['characteristicValue'],$row['val'],['class'=>'group-'.$groupId.'-row-val-'.$rowId.' form-control', 'style'=>'width:100%']) !!}
                                </div>
                            </div>
                        </div>
                    @endforeach
                </div>
                <div class="row-buttons-wrap">
                    <button class="create btn btn-primary js-create-new-row create-row">create new row</button>
                </div>
            </div>
            @endforeach
                @if (empty($productPresenter->characteristics))
                    <div class="group" data-group-id="1">
                        <button title="remove group" class="remove remove-group">x</button>
                        <div class="group-name-wrap">
                            <div class="form-group">
                                <label for="">Группа параметров</label>
                                {!! Form::select('charGroup[1][groupName]', $productPresenter->characteristicGroupName,null,['class'=>'group-select1 form-control', 'style'=>'width:100%']) !!}
                            </div>
                        </div>
                        <div class="rows"></div>
                        <div class="row-buttons-wrap">
                            <button class="create btn btn-primary js-create-new-row create-row">create new row</button>
                        </div>
                    </div>
                @endif
        </div>
    </div>
    <div class="group-buttons-wrap">
        <button class="create btn btn-primary js-create-new-group create-group">create group</button>
    </div>
</section>


<div class="clearfix"></div>
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('product.index') !!}" class="btn btn-default">Cancel</a>
</div>


