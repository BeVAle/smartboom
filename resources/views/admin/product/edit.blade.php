@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>Продукты</h1>
    </section>
    <div class="content">
        @include('adminlte-templates::common.errors')
        <div class="box box-primary">
            <div class="box-body">
                <div class="row">
                    {!! Form::model($productPresenter->product, ['route' => ['product.update', $productPresenter->product->id], 'method' => 'patch']) !!}
                    @include('admin.product.fields')
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
@endsection